package heaven.tool.ai.vietlott.constant;

public class Constant {
    //Nên tận dụng tối đa thread của java => nếu size list insert <1000 nên chia BATCH_SZIE = 200 thôi để insert lẹ
    // Nếu list data tầm 5000 => BATCH_SZIE  = 1000
    //Nếu list data tâm 50k=>200k => BATCH_SZIE = 10k
    public static final int BATCH_SIZE = 200;
    public static final int BATCH_SIZE_NORMAL = 500;
    public static final int BATCH_SIZE_MEDIUM = 1000;
    public static final int BATCH_SIZE_LARGE = 5000;
    public static final int BATCH_SIZE_HUGE = 10000;
    public static final int BATCH_SIZE_BIG_MOM = 40000;

    public static final int NUMBER_645 = 45;
    public static final int NUMBER_655 = 55;

    public static final String VIETLOTT_645_VALIDATE = "VIETLOTT_645_VALIDATE";
    public static final String VIETLOTT_655_VALIDATE = "VIETLOTT_655_VALIDATE";

    public static final String VIETLOTT_645_BY_DATE_DIFF = "VIETLOTT_645_BY_DATE_DIFF";
    public static final String VIETLOTT_655_BY_DATE_DIFF = "VIETLOTT_645_BY_DATE_DIFF";

    public static final String SEQ_ID = "SEQ_ID";
    public static final String NUMBER = "NUMBER";
    public static final String NUMBER_1 = "NUMBER_1";
    public static final String NUMBER_2 = "NUMBER_2";
    public static final String NUMBER_3 = "NUMBER_3";
    public static final String NUMBER_4 = "NUMBER_4";
    public static final String NUMBER_5 = "NUMBER_5";
    public static final String NUMBER_6 = "NUMBER_6";
    public static final String NUMBER_7 = "NUMBER_7";
    public static final String COUNT = "DISPLAY TIMES";
    public static final String DATE_RESULT = "DATE_RESULT";
    public static final String DATE_DIFF = "DATE_DIFF (Days)";

    public static final String CSV_SEPARATOR = ",";

    //VIETLOTT_6/45
    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_NUMBER_1 = "VIETLOTT_645_BY_NUMBER_1.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_NUMBER_2 = "VIETLOTT_645_BY_NUMBER_2.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_NUMBER_3 = "VIETLOTT_645_BY_NUMBER_3.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_NUMBER_4 = "VIETLOTT_645_BY_NUMBER_4.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_NUMBER_5 = "VIETLOTT_645_BY_NUMBER_5.csv";
    //VIETLOTT_6/55
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_1 = "VIETLOTT_655_BY_NUMBER_1.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_2 = "VIETLOTT_655_BY_NUMBER_2.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_3 = "VIETLOTT_655_BY_NUMBER_3.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_4 = "VIETLOTT_655_BY_NUMBER_4.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_5 = "VIETLOTT_655_BY_NUMBER_5.csv";

    //Export csv file with big data
    public static final String CSV_EXPORT_FILE_BIG_DATA = "VIETLOTT_645_BIG_DATA.csv";

    public static final String CSV_EXPORT_FILE_NAME_6_45_BY_DATE_DIFF = "GET_DATA_BY_DATE_DIFF_VIETLOTT_645.csv";
    public static final String CSV_EXPORT_FILE_NAME_6_55_BY_DATE_DIFF = "GET_DATA_BY_DATE_DIFF_VIETLOTT_655.csv";

    public static final String EXCEL_EXPORT_FILE_NAME = "export_result_VIETLOTT_645_";
    public static final String EXCEL_IMPORT_RESULT_NAME = "Import_Result_VIETLOTT_645_";

    public static final String SHEET_NAME = "VIETLOTT_645";

    public static final String VIETLOTT_645_HEADER ="SEQ_ID,NUMBER_1,NUMBER_2,NUMBER_3,NUMBER_4,NUMBER_5, NUMBER_6,DATE_RESULT";
}
