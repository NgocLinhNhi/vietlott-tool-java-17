package heaven.tool.ai.vietlott.mapper;

import org.springframework.jdbc.core.RowMapper;
import heaven.tool.ai.vietlott.model.VietLott655Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

@SuppressWarnings("rawtypes")
public class VietLott655Mapper implements RowMapper {

    @Override
    public VietLott655Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
        VietLott655Entity data = new VietLott655Entity();
        data.setPeriod(rs.getInt("SEQ_ID"));
        data.setNumber1(rs.getInt("number_1"));
        data.setNumber3(rs.getInt("number_3"));
        data.setNumber2(rs.getInt("number_2"));
        data.setNumber4(rs.getInt("number_4"));
        data.setNumber5(rs.getInt("number_5"));
        data.setNumber6(rs.getInt("number_6"));
        data.setCount(rs.getInt("count"));
        return data;
    }

}