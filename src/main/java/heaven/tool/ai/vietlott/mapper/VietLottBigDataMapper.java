package heaven.tool.ai.vietlott.mapper;

import org.springframework.jdbc.core.RowMapper;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VietLottBigDataMapper implements RowMapper<VietLott645Entity> {

    @Override
    public VietLott645Entity mapRow(ResultSet rs, int rowNum) throws SQLException {
        VietLott645Entity data = new VietLott645Entity();
        data.setPeriod(rs.getInt("SEQ_ID"));
        data.setNumber1(rs.getInt("number_1"));
        data.setNumber3(rs.getInt("number_3"));
        data.setNumber2(rs.getInt("number_2"));
        data.setNumber4(rs.getInt("number_4"));
        data.setNumber5(rs.getInt("number_5"));
        data.setNumber6(rs.getInt("number_6"));
        data.setDateOfResult(rs.getDate("DATE_RESULT"));
        return data;
    }


}