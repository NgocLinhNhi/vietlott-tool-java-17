package heaven.tool.ai.vietlott.thread;

import heaven.tool.ai.vietlott.concurrent.ThreadPoolLoop;
import heaven.tool.ai.vietlott.load_config.ExecutorConfig;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.ulti.MyPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static heaven.tool.ai.vietlott.constant.Constant.BATCH_SIZE_LARGE;

public class VietLottExecuteCallable {
    private final Logger logger = LoggerFactory.getLogger(VietLottExecuteCallable.class);

    private final Collection<Future<?>> futures = new LinkedList<>();

    public static VietLottExecuteCallable INSTANCE;

    public static VietLottExecuteCallable getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottExecuteCallable();
        return INSTANCE;
    }

    public void start(List<VietLott645Entity> listData,
                      int partitionSize,
                      String tableName) throws Exception {
        processHandleVietLottCallable(listData, partitionSize, tableName);
    }


    private void processHandleVietLottCallable(List<VietLott645Entity> listData,
                                               int batchSize,
                                               String tableName) throws Exception {
        //Nếu shutdown executor sẽ bị java.util.concurrent.RejectedExecutionException ngay
        // nếu không tạo mới lại ExecutorService cho request sau
        ExecutorService executorService = ThreadPoolLoop.getInstance().newExecutorService(ExecutorConfig.threadPoolSize);
        List<List<VietLott645Entity>> partition = partitionData(listData, batchSize);
        executeFuture(partition, tableName, executorService);
    }


    private List<List<VietLott645Entity>> partitionData(List<VietLott645Entity> listData, int batchSize) {
        return MyPartition.partition(listData, batchSize);
    }

    private void executeFuture(List<List<VietLott645Entity>> partition,
                               String tableName,
                               ExecutorService executorService) throws InterruptedException, ExecutionException {
        for (List<VietLott645Entity> lst : partition) {
            //nên cố định BATCH_SIZE_LARGE = 5000 khi insert Batch => nếu parttion bé (200) cũng không anh hưởng
            //Nhưng khi 1 Thread = 10.000 records (1 thread) thì insert Batch = 5000 1 hit tốt hơn
            // hoặc có thể giảm xuống 2000 1 hit vào DB
            VietLottCallable vietLottCallable = new VietLottCallable(lst, BATCH_SIZE_LARGE, tableName);
            Future<?> future = executorService.submit(vietLottCallable);
            // Từng Future sẽ quản lý một Callable
            futures.add(future);

        }

        for (Future<?> future : futures) {
            System.out.println("wait Thread");
            future.get();
        }

        executorService.shutdown();
        boolean finished = executorService.awaitTermination(1, TimeUnit.SECONDS);
        if (finished) logger.info("Finish generation");
        else logger.error("Generate report take more than one day. Stop");
    }
}
