package heaven.tool.ai.vietlott.thread;

import heaven.tool.ai.vietlott.dao.BatchHandleDao;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;

public class VietLotCallableHandler {

    public static VietLotCallableHandler getInstance() {
        return new VietLotCallableHandler();
    }

    String handlerProductCallable(
            List<VietLott645Entity> lst,
            int batchSize,
            String tableName) throws Exception {

        BatchHandleDao.getInstance().insertBatch(lst, batchSize, tableName);

        return "SUCCESS";
    }
}
