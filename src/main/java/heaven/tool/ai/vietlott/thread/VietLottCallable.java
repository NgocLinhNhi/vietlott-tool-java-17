package heaven.tool.ai.vietlott.thread;

import lombok.Getter;
import lombok.Setter;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;
import java.util.concurrent.Callable;

@Getter
@Setter
public class VietLottCallable implements Callable<String> {

    private List<VietLott645Entity> listData;
    private int batchSize;
    private String tableName;

    VietLottCallable(List<VietLott645Entity> listData,
                     int batchSize,
                     String tableName) {
        this.listData = listData;
        this.batchSize = batchSize;
        this.tableName = tableName;
    }

    @Override
    //Do implement Callable nên method Call sẽ tự được gọi khi gọi constructor
    public String call() throws Exception {
        return handlerProduct(this.listData, this.batchSize, this.tableName);
    }

    private String handlerProduct(List<VietLott645Entity> listData,
                                  int batchSize,
                                  String tableName) throws Exception {
        return VietLotCallableHandler.getInstance().handlerProductCallable(listData, batchSize, tableName);
    }
}
