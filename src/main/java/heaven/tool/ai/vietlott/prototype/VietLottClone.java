package heaven.tool.ai.vietlott.prototype;

import lombok.Getter;
import lombok.Setter;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;

import java.util.Date;

@Getter
@Setter
public class VietLottClone implements Cloneable {

    int number1;
    int number2;
    int number3;
    int number4;
    int number5;
    int number6;
    Date dateOfResult;
    int period;

    //prototype design pattern Clone 1 thằng A -> sang 1 thằng B
    public VietLottClone(VietLott655Entity obj) {
        this.number1 = obj.getNumber1();
        this.number2 = obj.getNumber2();
        this.number3 = obj.getNumber3();
        this.number4 = obj.getNumber4();
        this.number5 = obj.getNumber5();
        this.number6 = obj.getNumber6();
        this.dateOfResult = obj.getDateOfResult();
        this.period = obj.getPeriod();
    }

    public VietLott645Entity clone(VietLott655Entity obj) {
        return new VietLott645Entity(obj);
    }


    @Override
    public String toString() {
        return "VietLottClone{" +
                "number1=" + number1 +
                ", number2=" + number2 +
                ", number3=" + number3 +
                ", number4=" + number4 +
                ", number5=" + number5 +
                ", number6=" + number6 +
                ", dateOfResult=" + dateOfResult +
                ", period=" + period +
                '}';
    }
}
