package heaven.tool.ai.vietlott.prototype;

import lombok.Getter;
import lombok.Setter;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class VietLottData {
    //Để bao quát handle cho nó rộng => tạo 1 class Data chứa dữ liệu để handle cho chain of repository pattern
    List<VietLott645Entity> listVietLott645;
    List<VietLott655Entity> listVietLott655;

    protected final Map<String, Object> inputData;
    protected final Map<String, List<?>> stepDataMap;
    protected final Map<String, Map<Integer, Integer>> inputDataDateDiff;
    protected int dateDiff;
    protected VietLottType vietLottType;

    //Cac bước set vào constructor này = Composite design pattern
    public VietLottData(List<VietLott645Entity> vietlott645, List<VietLott655Entity> vietlott655) {
        this.listVietLott655 = vietlott655;
        this.listVietLott645 = vietlott645;

        this.inputData = new HashMap<>();
        this.stepDataMap = new HashMap<>();
        this.inputDataDateDiff = new HashMap<>();
    }

    public VietLottData(VietLottType vietLottType, int dateDiff) {
        this.dateDiff = dateDiff;
        this.vietLottType = vietLottType;

        this.inputData = new HashMap<>();
        this.stepDataMap = new HashMap<>();
        this.inputDataDateDiff = new HashMap<>();
    }

    //Các bước set data này là State design pattern
    //Cái này dùng để add lại data đã được xào nấu validate
    public void addInputData(String key, Object data) {
        this.inputData.put(key, data);
    }

    @SuppressWarnings("unchecked")
    public <T> T getInputData(String key) {
        Object data = inputData.get(key);
        if (data == null)
            throw new IllegalArgumentException("has no input data with key: " + key);
        return (T) data;
    }

    public void addInputDataDateDiff(String key, Map<Integer, Integer> data) {
        this.inputDataDateDiff.put(key, data);
    }

    public Map<Integer, Integer> getInputDataDateDiff(String key) {
        Map<Integer, Integer> data = inputDataDateDiff.get(key);
        if (data == null)
            throw new IllegalArgumentException("has no input data with key: " + key);
        return data;
    }

    //Cái này dùng để add lại data đã được xào nấu validate
    public void addStepData(String key, List<?> data) {
        this.stepDataMap.put(key, data);
    }

    public List getStepData(String key) {
        List data = stepDataMap.get(key);
        if (data == null)
            throw new IllegalArgumentException("has no step data with key: " + key + ", it may has removed");
        return data;
    }

}
