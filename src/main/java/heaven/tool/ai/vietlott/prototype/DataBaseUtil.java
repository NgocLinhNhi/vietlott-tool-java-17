package heaven.tool.ai.vietlott.prototype;

import heaven.tool.ai.vietlott.model.VietLott655Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.ArrayList;
import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public class DataBaseUtil {
    private static final Logger logger = LoggerFactory.getLogger(DataBaseUtil.class);

    public static String getTableName(VietLottType vietLottType) {
        return vietLottType == VietLottType.VIETLOTT645 ? "VIETLOTT_645" : "VIETLOTT_655";
    }

    public static List<VietLott645Entity> cloneVietLott655(List<VietLott655Entity> listData) {
        long timeStart = System.currentTimeMillis();
        logger.info("Time Start for clone list  data VietLott 6/55: {}", timeStart);
        List<VietLott645Entity> listClone = new ArrayList<>();
        for (VietLott655Entity data : listData) {
            VietLottClone vietLottClone = new VietLottClone(data);
            listClone.add(vietLottClone.clone(data));
        }
        long timeEnd = System.currentTimeMillis();
        logger.info("Time End for handle clone Data VietLott 655: {}", timeEnd);
        logger.info("Total time for clone data: {} ms (1s=1000ms)", (timeEnd - timeStart));
        //639 data = 3ms cho clone 1 list object => tuy nhiên
        //sẽ tốn khá nhiều time cho clone 1 list vài chục k - vài trăm k data đó => ví dụ cho có prototype design pattern thôi
        // cần nhắc khi cần clone cả 1 List<Object> nhé !!!
        return listClone;
    }

    public static int getBatchSize(List<VietLott645Entity> listData) {
        if (listData.size() < BATCH_SIZE_MEDIUM) {
            logger.info("Information of BATCH_SIZE {} ", BATCH_SIZE);
            return BATCH_SIZE;
        } else if (listData.size() > BATCH_SIZE_MEDIUM && listData.size() < BATCH_SIZE_LARGE) {
            logger.info("Information of BATCH_SIZE {} ", BATCH_SIZE_MEDIUM);
            return BATCH_SIZE_NORMAL;
        } else if (listData.size() > BATCH_SIZE_LARGE && listData.size() < BATCH_SIZE_BIG_MOM) {
            logger.info("Information of BATCH_SIZE {} ", BATCH_SIZE_LARGE);
            return BATCH_SIZE_LARGE;
        }
        logger.info("Information of BATCH_SIZE {} ", BATCH_SIZE_HUGE);
        return BATCH_SIZE_HUGE;
    }
}
