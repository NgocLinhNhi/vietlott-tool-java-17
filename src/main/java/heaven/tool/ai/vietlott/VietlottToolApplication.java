package heaven.tool.ai.vietlott;

import heaven.tool.ai.vietlott.startup.VietLottToolStartUp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VietlottToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(VietlottToolApplication.class, args);
        VietLottToolStartUp.getInstance().start();
    }

}
