package heaven.tool.ai.vietlott.vietlott_chain_of_repository;

import heaven.tool.ai.vietlott.interfaces.VietLottChainOfHandlers;
import heaven.tool.ai.vietlott.interfaces.VietLottSimpleChainOfHandlers;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.VIETLOTT_645_VALIDATE;
import static heaven.tool.ai.vietlott.constant.Constant.VIETLOTT_655_VALIDATE;

public class VietLottSelectChainByServiceType {

    public static VietLottSelectChainByServiceType getInstance() {
        return new VietLottSelectChainByServiceType();
    }

    //Chain of Repository design pattern
    public VietLottChainOfHandlers newVietLottChainOfHandlers(VietLottType serviceType) {
        VietLottSimpleChainOfHandlers answer = VietLottSimpleChainOfHandlers.getInstance();
        if (serviceType == VietLottType.VIETLOTT645)
            answer.addVietLott645Handlers();
        else
            answer.addVietLott655Handlers();
        return answer;
    }

    public VietLottChainOfHandlers getDataByDateDiffChainOfHandlers(VietLottType serviceType) {
        VietLottSimpleChainOfHandlers answer = VietLottSimpleChainOfHandlers.getInstance();
        if (serviceType == VietLottType.VIETLOTT645)
            answer.addData645ByDateDiffHandlers();
        else
            answer.addData655ByDateDiffHandlers();
        return answer;
    }

    public VietLottData newVietLottData(List<VietLott645Entity> vietlott645, List<VietLott655Entity> vietlott655) {
        //C1 : add = get/set
        VietLottData vietLottData = new VietLottData(vietlott645, vietlott655);
        //C2: add vào stepData hoặc inputData
        vietLottData.addStepData(VIETLOTT_655_VALIDATE, vietlott655);
        vietLottData.addStepData(VIETLOTT_645_VALIDATE, vietlott645);
        return vietLottData;
    }

    public VietLottData newVietLottDataByDateDiff(VietLottType vietLottType, int dateDiff) {
        return new VietLottData(vietLottType, dateDiff);
    }
}
