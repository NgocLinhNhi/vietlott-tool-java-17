package heaven.tool.ai.vietlott.vietlott_chain_of_repository;

import heaven.tool.ai.vietlott.interfaces.VietLottHandler;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import heaven.tool.ai.vietlott.ulti.ExportDataUtil;
import heaven.tool.ai.vietlott.ulti.SortCollectionUtil;
import heaven.tool.ai.vietlott.ulti.csv.SimpleCsvPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.LinkedHashMap;
import java.util.Map;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public class VietLottStep2GetDataNearestByDateDiff implements VietLottHandler {

    private final Logger logger = LoggerFactory.getLogger(VietLottStep2GetDataNearestByDateDiff.class);

    @Override
    public void handle(VietLottData request) {
        long timeStart = System.currentTimeMillis();
        logger.info("Start step 2 process export file csv - data VIETLOTT nearest by date Diff : {}", timeStart);
        int dateDiff = request.getDateDiff();
        VietLottType vietLottType = request.getVietLottType();
        Map<Integer, Integer> inputDataByVietLottType = getInputDataByVietLottType(request, vietLottType);
        exportToFileGetDataByDateDiff(inputDataByVietLottType, dateDiff, vietLottType);
        long timeEnd = System.currentTimeMillis();
        logger.info("End step 2 process export file csv - data VIETLOTT nearest by date Diff : {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }

    private void exportToFileGetDataByDateDiff(Map<Integer, Integer> inputDataDateDiff,
                                               int dateDiff,
                                               VietLottType vietLottType) {
        //sort Map đứa cặp số ra nhiều nhất lên đầu
        LinkedHashMap<Integer, Integer> sortedLinkedHashMap = SortCollectionUtil.sortHashMapDesc(inputDataDateDiff);
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(getFileName(vietLottType))) {
            printer.write(ExportDataUtil.buildHeader(DATE_DIFF, NUMBER).toString());
            for (Map.Entry<Integer, Integer> entry : sortedLinkedHashMap.entrySet()) {
                printer.write(
                        dateDiff + "(Days)",
                        String.valueOf(entry.getKey()),
                        String.valueOf(entry.getValue())
                );
            }
            printer.flush();
            inputDataDateDiff.clear();
        }
    }

    private Map<Integer, Integer> getInputDataByVietLottType(VietLottData request, VietLottType vietLottType) {
        if (vietLottType == VietLottType.VIETLOTT645)
            return request.getInputDataDateDiff(VIETLOTT_645_BY_DATE_DIFF);
        return request.getInputDataDateDiff(VIETLOTT_655_BY_DATE_DIFF);
    }

    private String getFileName(VietLottType vietLottType) {
        if (vietLottType == VietLottType.VIETLOTT645)
            return ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_45_BY_DATE_DIFF);
        return ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_DATE_DIFF);
    }
}
