package heaven.tool.ai.vietlott.vietlott_chain_of_repository;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.interfaces.VietLottHandler;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;

public class VietLott645Step2TruncateData implements VietLottHandler {
    private final Logger logger = LoggerFactory.getLogger(VietLott645Step2TruncateData.class);

    @Override
    public void handle(VietLottData data) throws Exception {
        logger.info("Begin truncate table VietLott645 in Database");
        VietLottAbstractFactory vietLottByType = BusinessManageDaoFactory.getInstance().getVietLottByType(VietLottType.VIETLOTT645);
        vietLottByType.createVietLott().truncateData();
        logger.info("Truncate table VietLott_645 successfully");
    }

}
