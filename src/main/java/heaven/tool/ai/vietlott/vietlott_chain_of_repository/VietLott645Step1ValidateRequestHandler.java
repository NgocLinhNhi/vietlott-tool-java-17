package heaven.tool.ai.vietlott.vietlott_chain_of_repository;

import heaven.tool.ai.vietlott.interfaces.VietLottHandler;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import heaven.tool.ai.vietlott.ulti.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;

public class VietLott645Step1ValidateRequestHandler implements VietLottHandler {

    private final Logger logger = LoggerFactory.getLogger(VietLott645Step1ValidateRequestHandler.class);

    @Override
    public void handle(VietLottData request) throws Exception {
        List<VietLott645Entity> listVietLott645 = request.getListVietLott645();
        logger.info("start validate  data for VietLott645 request has {} records", listVietLott645.size());
        String result = validatData(listVietLott645);
        if (!StringUtil.isEmpty(result)) {
            throw new Exception("Validate File has Error");
        }
    }

    private String validatData(List<VietLott645Entity> request) {
        logger.info("Validate data {}", request);
        return null;
    }
}
