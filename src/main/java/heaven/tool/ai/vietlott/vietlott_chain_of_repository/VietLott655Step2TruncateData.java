package heaven.tool.ai.vietlott.vietlott_chain_of_repository;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.interfaces.VietLottHandler;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.VIETLOTT_655_VALIDATE;

public class VietLott655Step2TruncateData implements VietLottHandler {
    private final Logger logger = LoggerFactory.getLogger(VietLott655Step2TruncateData.class);

    @SuppressWarnings("unchecked")
    @Override
    public void handle(VietLottData data) throws Exception {
        //Ví dụ steps 2 là insert data chẳng hạn get lại List<T> data được xào nấu từ steps 1
        List<VietLott655Entity> lstData = data.getStepData(VIETLOTT_655_VALIDATE);
        logger.info("List Data after validate {}", lstData);

        logger.info("Begin truncate table VietLott645 in Database");
        VietLottAbstractFactory vietLottByType = BusinessManageDaoFactory.getInstance().getVietLottByType(VietLottType.VIETLOTT655);
        vietLottByType.createVietLott().truncateData();
        logger.info("Truncate table VietLott_655 successfully");
    }

}
