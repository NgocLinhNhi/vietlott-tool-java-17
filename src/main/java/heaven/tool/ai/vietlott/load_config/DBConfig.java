package heaven.tool.ai.vietlott.load_config;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class DBConfig {
    private static DBConfig INSTANCE;
    private static JdbcTemplate jdbcTemplate;
    private static NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public static DBConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new DBConfig();
        return INSTANCE;
    }

    public JdbcTemplate getConnection() throws Exception {
        //flyweight design pattern only 1 object tồn tại in project
        if (jdbcTemplate == null)
            jdbcTemplate = new JdbcTemplate(PersistenceJPAConfig.getInstance().dataSource());

        return jdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedConnection() throws Exception {
        if (namedParameterJdbcTemplate == null)
            namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(PersistenceJPAConfig.getInstance().dataSource());
        return namedParameterJdbcTemplate;
    }
}