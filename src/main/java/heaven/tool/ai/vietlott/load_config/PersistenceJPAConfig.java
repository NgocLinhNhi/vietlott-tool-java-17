package heaven.tool.ai.vietlott.load_config;

import heaven.tool.ai.vietlott.ulti.io.InputStreams;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class PersistenceJPAConfig {
    //File này sẽ tự load khi run class VietLottApplication
    //Table sẽ được tạo theo config định nghĩa trong dto

    private static PersistenceJPAConfig INSTANCE;

    public static PersistenceJPAConfig getInstance() {
        if (INSTANCE == null) INSTANCE = new PersistenceJPAConfig();
        return INSTANCE;
    }


    //C1: cách khác config SPringJPA = tay
    //cái này sẽ không đọc auto config từ file properties mà là config = tay
    //C2: project Spring-Batch cũng dùng jdbc-Template nhưng config = xml
    //C3: dùng tự load các properties của SpringBoot và @autoWire Datasource để tự load config trong file application.properties
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan("heaven.tool.ai.vietlott.dto");

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    //khởi tạo config database = datasource
    @Bean
    public DataSource dataSource() throws Exception {
        //builder pattern => log se full thông tin datasource
//        return DataSourceBuilder.create()
//                .driverClassName(getInstance().loadProperties().getProperty("spring.datasource.driver-class-name"))
//                .url(getInstance().loadProperties().getProperty("spring.datasource.url"))
//                .username(getInstance().loadProperties().getProperty("spring.datasource.username"))
//                .password(getInstance().loadProperties().getProperty("spring.datasource.password"))
//                .build();
        //C2 :
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(getInstance().loadProperties().getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(getInstance().loadProperties().getProperty("spring.datasource.url"));
        dataSource.setUsername(getInstance().loadProperties().getProperty("spring.datasource.username"));
        dataSource.setPassword(getInstance().loadProperties().getProperty("spring.datasource.password"));
        return dataSource;
    }

    //load File Properties
    private Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        properties.put("spring.datasource.driver-class-name", properties.get("spring.datasource.driver-class-name"));
        properties.put("spring.datasource.url", properties.get("spring.datasource.url"));
        properties.put("spring.datasource.username", properties.get("spring.datasource.username"));
        properties.put("spring.datasource.password", properties.get("spring.datasource.password"));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("jms.properties.file");
        if (file == null)
            file = "application.properties";
        return file;
    }
    //load File Properties

    @Bean
    public PlatformTransactionManager transactionManager() throws Exception {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    private Properties additionalProperties() {
        Properties properties = new Properties();
        // ko nên để create-drop => no drop table khi error đó =.=
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

        return properties;
    }
}
