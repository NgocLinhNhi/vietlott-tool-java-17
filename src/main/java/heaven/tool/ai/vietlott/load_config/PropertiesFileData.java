package heaven.tool.ai.vietlott.load_config;

import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.ulti.CsvReaderUtil;
import heaven.tool.ai.vietlott.ulti.io.InputStreams;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class PropertiesFileData {

    private static PropertiesFileData INSTANCE;

    public static PropertiesFileData getInstance() {
        if (INSTANCE == null) INSTANCE = new PropertiesFileData();
        return INSTANCE;
    }

    //Csv - file
    public List<VietLott645Entity> getListData645FromCsvFile() throws Exception {
        return CsvReaderUtil.getInstance().readDataVietLott645(getInstance().getFilePathVietLott645());
    }

    //Csv - file
    public List<VietLott655Entity> getListData655FromCsvFile() throws Exception {
        return CsvReaderUtil.getInstance().readDataVietLott655(getInstance().getFilePathVietLott655());
    }

    //Excel - file
    public String getFilePathExcelVietLott645() throws Exception {
        return getInstance().loadProperties().getProperty("file.path.excel.645");
        //return getInstance().loadProperties().getProperty("file.path.excel.645_VALIDATE");
    }

    private String getFilePathVietLott645() throws Exception {
        return getInstance().loadProperties().getProperty("file.path.645");
        //return getInstance().loadProperties().getProperty("file.path.645_1k_data");
        //return getInstance().loadProperties().getProperty("file.path.645_30k_data");
        //return getInstance().loadProperties().getProperty("file.path.645_100k_data");
        //return getInstance().loadProperties().getProperty("file.path.645_400k_data");

    }

    private String getFilePathVietLott655() throws Exception {
        return getInstance().loadProperties().getProperty("file.path.655");
    }

    public Properties loadProperties() throws Exception {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(getPropertiesFile()));
        return properties;
    }

    private String getPropertiesFile() {
        String file = System.getProperty("jms.properties.file");
        if (file == null)
            file = "application.properties";
        return file;
    }

    //Cách khác để đọc file properties => Code từ DTS Project ko cần InpuStream
    //Code RSBO chỉ là tách class ra từ đây
    private static String getFileProperties() throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream input = classLoader.getResourceAsStream("application.properties");
        Properties prop = new Properties();
        prop.load(input);
        return prop.getProperty("file.path.645");
    }

}
