package heaven.tool.ai.vietlott.load_config;

public class SQLQuerry {
    public static final String TRUNCATE_TABLE_VIETLOTT_645 = "TRUNCATE TABLE VIETLOTT_645";
    public static final String TRUNCATE_TABLE_VIETLOTT_655 = "TRUNCATE TABLE VIETLOTT_655";

    public static StringBuilder InsertVietLottSQL(String tableName) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO                          ")
                .append(tableName)
                .append("    (SEQ_ID,                    ")
                .append("    NUMBER_1,                   ")
                .append("    NUMBER_2,                   ")
                .append("    NUMBER_3,                   ")
                .append("    NUMBER_4,                   ")
                .append("    NUMBER_5,                   ")
                .append("    NUMBER_6,                   ")
                .append("    DATE_RESULT,                ")
                .append("    DATE_INSERT)                ")
                .append("    VALUES(?,?,?,?,?,?,?,?,?)   ");
        return sql;
    }

    public static StringBuilder loadAllData(String tableName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM  ");
        sql.append(tableName);
        return sql;
    }

    public static StringBuilder loadAllDataByLimitOffSet(String tableName, int limit, int offset) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM       ");
        sql.append(tableName);
        sql.append("  LIMIT         ");
        sql.append(limit);
        sql.append(",");
        sql.append(offset);
        return sql;
    }

    public static String searchNumberByNumber(String tableName,
                                              String numberCondition,
                                              String numberOrder) {
        StringBuilder sql = new StringBuilder();
        sql.append("    SELECT                             ");
        sql.append("        SEQ_ID,                        ");
        sql.append("        NUMBER_1,                      ");
        sql.append("        NUMBER_2,                      ");
        sql.append("        NUMBER_3,                      ");
        sql.append("        NUMBER_4,                      ");
        sql.append("        NUMBER_5,                      ");
        sql.append("        NUMBER_6,                      ");
        sql.append("        COUNT(*) as count              ");
        sql.append("    FROM                               ");
        sql.append(tableName);
        sql.append("    WHERE                              ");
        sql.append(numberCondition);
        sql.append("         = ?                           ");
        sql.append("        GROUP BY                       ");
        sql.append(numberOrder);
        sql.append("        ORDER BY count desc            ");
        sql.append("        limit 10;                      ");
        return sql.toString();
    }


    //C1 : set parameter 1 kiểu truyền thẳng vào câu lệnh sql
    // cách này dùng JdbcTemplate => nhược điểm nhìn câu sql do append tham số hơi rối mắt
    public static StringBuilder findNearestResultByDateDiff(String tableName, int numberInput, int dateDiff) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT                                                  ");
        sql.append("   SUM(A.count) AS totalCount                           ");
        sql.append(" FROM                                                    ");
        sql.append("   (SELECT                                              ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                               ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_1 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=              ");
        sql.append(dateDiff);
        sql.append(" UNION                                                   ");
        sql.append("    SELECT                                              ");
        sql.append("        COUNT(*) AS count                               ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                              ");
        sql.append("       NUMBER_2 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=              ");
        sql.append(dateDiff);
        sql.append(" UNION                                                   ");
        sql.append("    SELECT                                               ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                 ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_3 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=              ");
        sql.append(dateDiff);
        sql.append(" UNION                                                    ");
        sql.append("    SELECT                                              ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_4 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=              ");
        sql.append(dateDiff);
        sql.append(" UNION                                                    ");
        sql.append("    SELECT                                               ");
        sql.append("       COUNT(*) AS count                                 ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                                ");
        sql.append("       NUMBER_5 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=              ");
        sql.append(dateDiff);
        sql.append(" UNION                                                   ");
        sql.append("    SELECT                                              ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_6 =                                       ");
        sql.append(numberInput);
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <=             ");
        sql.append(dateDiff);
        sql.append("    ) AS A;                                             ");

        return sql;
    }

    //C2 : set parameter 1 kiểu cho nhiều vị trí không thể set như bt được
    // cách này dùng NamedParameterJdbcTemplate => ưu điểm nhìn câu sql không rối mắt
    public static StringBuilder findNearestResultByDateDiff(String tableName) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT                                                  ");
        sql.append("   SUM(A.count) AS totalCount                           ");
        sql.append("FROM                                                    ");
        sql.append("   (SELECT                                              ");
        sql.append("       *,                                               ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                               ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_1 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff    ");
        sql.append("UNION                                                   ");
        sql.append("    SELECT                                              ");
        sql.append("        *,                                              ");
        sql.append("        COUNT(*) AS count                               ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                              ");
        sql.append("       NUMBER_2 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff    ");
        sql.append("UNION                                                   ");
        sql.append("    SELECT                                               ");
        sql.append("       *,                                                ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                 ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_3 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff    ");
        sql.append("UNION                                                   ");
        sql.append("    SELECT                                              ");
        sql.append("       *,                                               ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_4 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff    ");
        sql.append("UNION                                                    ");
        sql.append("    SELECT                                              ");
        sql.append("       *,                                               ");
        sql.append("       COUNT(*) AS count                                 ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_5 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff     ");
        sql.append("UNION                                                   ");
        sql.append("    SELECT                                              ");
        sql.append("       *,                                               ");
        sql.append("       COUNT(*) AS count                                ");
        sql.append("    FROM                                                ");
        sql.append(tableName);
        sql.append("    WHERE                                               ");
        sql.append("       NUMBER_6 = :numberInput                          ");
        sql.append("       AND DATEDIFF(NOW(), DATE_RESULT) <= :dateDiff    ");
        sql.append("    ) AS A;                                             ");

        return sql;
    }
}
