package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.vietlott_chain_of_repository.VietLottSelectChainByServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

//bên ngoài sử dụng template method
public class VietLott645InsertHandler extends VietLott645InsertRequestHandler<VietLott645Entity> {

    private final Logger logger = LoggerFactory.getLogger(VietLott645InsertHandler.class);

    //Validate for request here
    @Override
    protected void preHandle(List<VietLott645Entity> request) throws Exception {
        //Thật ra nên dùng 1 trong 2 thôi ( requestHandler or Chain Of Repository design pattern)
        // => requestHandler => xử lý message (JMS - activeMQ)
        // => Chain Of Repository design pattern => xử lý insert data / update => gồm nhiều steps validate + insert
        // => Chain Of Repository design pattern => xử lý deposit / withdraw
        logger.info("start validate  data for VietLott 6/45 request {}", request);
        //Bên trong sử dụng thêm chains of repository design pattern T_T
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        var handlers = instance.newVietLottChainOfHandlers(VietLottType.VIETLOTT645);
        handlers.handle(VietLottSelectChainByServiceType.getInstance().newVietLottData(request, null));
    }

    @Override
    protected void doHandle(List<VietLott645Entity> request) throws Exception {
        insertDataVietLott645(request);
        logger.info("Insert data for VietLott645 has successfully with {} records", request.size());
    }

    @Override
    protected void postHandle(List<VietLott645Entity> request) {
        logger.info("end handle request for VietLott 6/45 !!!");
    }


}
