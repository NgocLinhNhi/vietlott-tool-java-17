package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.ulti.csv.SimpleCsvPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.constant.Constant;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.ulti.ExportDataUtil;
import heaven.tool.ai.vietlott.ulti.date.DateUtil;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.CSV_EXPORT_FILE_BIG_DATA;
import static heaven.tool.ai.vietlott.constant.DateConstant.PATTERN_YY_MM_DD;

public class VietLott645ExportBigData {
    private final Logger logger = LoggerFactory.getLogger(VietLott645ExportBigData.class);

    private static VietLott645ExportBigData INSTANCE;

    public static VietLott645ExportBigData getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLott645ExportBigData();
        return INSTANCE;
    }

    private IVietLottDAO createDbProcessor(VietLottType vietLottType) {
        BusinessManageDaoFactory instance = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory factory = instance.getVietLottByType(vietLottType);
        return factory.createVietLott();
    }

    //Handle Export Csv With big data - No use Limit offset
    void exportBigDataNoLimitOffset(VietLottType vietLottType) throws Exception {
        logger.info("Start export csv process with BIG DATA - Dont Use Limit OFFSET !!! ");
        List<VietLott645Entity> listData = getListVietLott645BigDataNormal(vietLottType);
        logger.info("List Data {} records", listData.size());
        exportToFileCsvBigDataNormal(listData);
        logger.info("End Time export cvs process with BIG DATA - Dont Use Limit OFFSET !!!");
    }

    private List<VietLott645Entity> getListVietLott645BigDataNormal(VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return dbProcessor.loadAllData(vietLottType);
    }

    private void exportToFileCsvBigDataNormal(List<VietLott645Entity> listData) {
        try (SimpleCsvPrinter printer = createFileCsv()) {
            exportContentCsvFile(listData, printer);
            printer.flush();
            listData.clear();
        }
    }

    //Handle Export Csv With big data = use Limit offset
    void exportBigDataUseLimitOffSet(VietLottType vietLottType) throws Exception {
        logger.info("START EXPORT CSV PROCESS WITH BIG DATA - USER LIMIT / OFFSET !!! ");
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        exportCsvFileBigDataProcess(vietLottType, dbProcessor);
        logger.info("END TIME EXPORT CVS PROCESS WITH BIG DATA - USER LIMIT / OFFSET !!! ");
    }

    private List<VietLott645Entity> getListVietLott645BigDataLimitOffSet(VietLottType vietLottType,
                                                                         int from,
                                                                         int offSet,
                                                                         IVietLottDAO dbProcessor) throws Exception {
        List<VietLott645Entity> listData = dbProcessor.loadAllDataByLimitOffSet(vietLottType, from, offSet);
        logger.info("Get VietLott 6/45 Data list {}", listData.size());
        return listData;
    }

    private void exportCsvFileBigDataProcess(VietLottType vietLottType, IVietLottDAO dbProcessor) throws Exception {
        try (SimpleCsvPrinter printWriter = createFileCsv()) {
            int from = 0; // Từ records bao nhiêu
            int offSet = Constant.BATCH_SIZE_HUGE; // mỗi lần lấy ra bao nhiêu bản ghi
            while (true) {
                List<VietLott645Entity> listData = getListVietLott645BigDataLimitOffSet(
                        vietLottType,
                        from,
                        offSet,
                        dbProcessor);

                from += offSet;
                exportContentCsvFile(listData, printWriter);
                if (listData.size() < offSet) break;
            }
        }
    }

    private SimpleCsvPrinter createFileCsv() {
        SimpleCsvPrinter printer = new SimpleCsvPrinter(ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_BIG_DATA));
        printer.write(ExportDataUtil.buildHeaderBigData().toString());
        return printer;
    }

    private void exportContentCsvFile(List<VietLott645Entity> listData, SimpleCsvPrinter printer) {
        for (VietLott645Entity data : listData) {
            printer.write(
                    String.valueOf(data.getPeriod()),
                    String.valueOf(data.getNumber1()),
                    String.valueOf(data.getNumber2()),
                    String.valueOf(data.getNumber3()),
                    String.valueOf(data.getNumber4()),
                    String.valueOf(data.getNumber5()),
                    String.valueOf(data.getNumber6()),
                    DateUtil.toString(data.getDateOfResult(), PATTERN_YY_MM_DD)
            );
        }
    }
}
