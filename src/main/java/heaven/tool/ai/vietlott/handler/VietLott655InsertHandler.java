package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.vietlott_chain_of_repository.VietLottSelectChainByServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class VietLott655InsertHandler extends VietLott655InsertRequestHandler<VietLott655Entity> {

    private final Logger logger = LoggerFactory.getLogger(VietLott655InsertHandler.class);

    @Override
    protected void preHandle(List<VietLott655Entity> request) throws Exception {
        logger.info("start validate  data for VietLott 6/55 request {}", request);
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        var handlers = instance.newVietLottChainOfHandlers(VietLottType.VIETLOTT655);
        handlers.handle(VietLottSelectChainByServiceType.getInstance().newVietLottData(null, request));
    }

    @Override
    protected void doHandle(List<VietLott655Entity> request) throws Exception {
        insertDataVietLott655(request);
        logger.info("Insert data for VietLott655 has successfully with {} records", request.size());
    }

    @Override
    protected void postHandle(List<VietLott655Entity> request) {
        logger.info("end handle request for VietLott 6/55 {} records ", request.size());
    }


}
