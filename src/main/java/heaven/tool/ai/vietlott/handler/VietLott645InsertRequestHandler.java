package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;

abstract class VietLott645InsertRequestHandler<T> extends RequestHandler<T> {

    void insertDataVietLott645(List<VietLott645Entity> request) throws Exception {
        BusinessManageDaoFactory factory = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory vietLottByType = factory.getVietLottByType(VietLottType.VIETLOTT645);
        IVietLottDAO vietLottDao = vietLottByType.createVietLott();
        vietLottDao.insertDataVietLott645(request);
    }

}
