package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.List;

abstract class VietLott655InsertRequestHandler<T> extends RequestHandler<T> {

    void insertDataVietLott655(List<VietLott655Entity> request) throws Exception {
        BusinessManageDaoFactory factory = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory vietLottByType = factory.getVietLottByType(VietLottType.VIETLOTT655);
        IVietLottDAO vietLottDao = vietLottByType.createVietLott();
        vietLottDao.insertDataVietLott655(request);
    }

}
