package heaven.tool.ai.vietlott.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

//Template method design pattern
public abstract class RequestHandler<T> {
    private final Logger logger = LoggerFactory.getLogger(RequestHandler.class);

    public final void handle(List<T> request) {
        try {
            preHandle(request);
            doHandle(request);
            postHandle(request);
        } catch (Exception e) {
            logger.error("handle request: {}, ", request, e);
        }
    }

    protected void preHandle(List<T> request) throws Exception {
    }

    protected void postHandle(List<T> request) {
    }

    protected abstract void doHandle(List<T> request) throws Exception;
}
