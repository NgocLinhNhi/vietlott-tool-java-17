package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.ulti.csv.SimpleCsvPrinter;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.ulti.ExportDataUtil;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.*;

abstract class VietLott655ViewDataRequestHandler extends ViewDataRequestHandler {

    private IVietLottDAO createDbProcessor(VietLottType vietLottType) {
        BusinessManageDaoFactory instance = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory factory = instance.getVietLottByType(vietLottType);
        return factory.createVietLott();
    }

    void executeViewData655ByNumber1(int number1, VietLottType vietLottType) throws Exception {
        List<VietLott655Entity> lstDataNumber2 = getList655ViewNumber2ByNumber1(number1, vietLottType);
        List<VietLott655Entity> lstDataNumber3 = getList655ViewNumber3ByNumber1(number1, vietLottType);
        List<VietLott655Entity> lstDataNumber4 = getList655ViewNumber4ByNumber1(number1, vietLottType);
        List<VietLott655Entity> lstDataNumber5 = getList655ViewNumber5ByNumber1(number1, vietLottType);
        List<VietLott655Entity> lstDataNumber6 = getList655ViewNumber6ByNumber1(number1, vietLottType);
        exportCsvFileByNumber1(lstDataNumber2, lstDataNumber3, lstDataNumber4, lstDataNumber5, lstDataNumber6);
    }

    void executeViewData655ByNumber2(int number2, VietLottType vietLottType) throws Exception {
        List<VietLott655Entity> lstDataNumber3 = getList655ViewNumber3ByNumber2(number2, vietLottType);
        List<VietLott655Entity> lstDataNumber4 = getList655ViewNumber4ByNumber2(number2, vietLottType);
        List<VietLott655Entity> lstDataNumber5 = getList655ViewNumber5ByNumber2(number2, vietLottType);
        List<VietLott655Entity> lstDataNumber6 = getList655ViewNumber6ByNumber2(number2, vietLottType);
        exportCsvFileByNumber2(lstDataNumber3, lstDataNumber4, lstDataNumber5, lstDataNumber6);
    }

    void executeViewData655ByNumber3(int number3, VietLottType vietLottType) throws Exception {
        List<VietLott655Entity> lstDataNumber4 = getList655ViewNumber4ByNumber3(number3, vietLottType);
        List<VietLott655Entity> lstDataNumber5 = getList655ViewNumber5ByNumber3(number3, vietLottType);
        List<VietLott655Entity> lstDataNumber6 = getList655ViewNumber6ByNumber3(number3, vietLottType);
        exportCsvFileByNumber3(lstDataNumber4, lstDataNumber5, lstDataNumber6);
    }

    void executeViewData655ByNumber4(int number4, VietLottType vietLottType) throws Exception {
        List<VietLott655Entity> lstDataNumber5 = getList655ViewNumber5ByNumber4(number4, vietLottType);
        List<VietLott655Entity> lstDataNumber6 = getList655ViewNumber6ByNumber4(number4, vietLottType);
        exportCsvFileByNumber4(lstDataNumber5, lstDataNumber6);
    }

    void executeViewData655ByNumber5(int number5, VietLottType vietLottType) throws Exception {
        List<VietLott655Entity> lstDataNumber6 = getList655ViewNumber6ByNumber5(number5, vietLottType);
        exportCsvFileByNumber5(lstDataNumber6);
    }

    //View Data by Number 2
    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber2ByNumber1(int number1,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber2ByNumber1(vietLottType, number1);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber3ByNumber1(int number1,
                                                                   VietLottType vietLottType) throws Exception {
        VietLottAbstractFactory vietLottByType = BusinessManageDaoFactory.getInstance().getVietLottByType(vietLottType);
        return (List<VietLott655Entity>) vietLottByType.createVietLott().selectNumber3ByNumber1(vietLottType, number1);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber4ByNumber1(int number1,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber4ByNumber1(vietLottType, number1);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber5ByNumber1(int number1,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber5ByNumber1(vietLottType, number1);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber6ByNumber1(int number1,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber6ByNumber1(vietLottType, number1);
    }


    //View Data by Number 2
    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber3ByNumber2(int number2,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber3ByNumber2(vietLottType, number2);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber4ByNumber2(int number2,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber4ByNumber2(vietLottType, number2);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber5ByNumber2(int number2,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber5ByNumber2(vietLottType, number2);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber6ByNumber2(int number2,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber6ByNumber2(vietLottType, number2);
    }

    //View Data by Number 3
    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber4ByNumber3(int number3,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber4ByNumber3(vietLottType, number3);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber5ByNumber3(int number3,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber5ByNumber3(vietLottType, number3);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber6ByNumber3(int number3,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber6ByNumber3(vietLottType, number3);
    }

    //View Data by Number 4
    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber5ByNumber4(int number4,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber5ByNumber4(vietLottType, number4);
    }

    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber6ByNumber4(int number4,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber6ByNumber4(vietLottType, number4);
    }

    //View Data by Number 5
    @SuppressWarnings("unchecked")
    private List<VietLott655Entity> getList655ViewNumber6ByNumber5(int number5,
                                                                   VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return (List<VietLott655Entity>) dbProcessor.selectNumber6ByNumber5(vietLottType, number5);
    }

    //Business export file
    private void exportCsvFileByNumber1(List<VietLott655Entity> listDataNumber2,
                                        List<VietLott655Entity> listDataNumber3,
                                        List<VietLott655Entity> listDataNumber4,
                                        List<VietLott655Entity> listDataNumber5,
                                        List<VietLott655Entity> listDataNumber6) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(
                ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_1))) {
            printer.write(ExportDataUtil.buildHeader(NUMBER_1, NUMBER_2).toString());
            for (VietLott655Entity data : listDataNumber2) {
                printer.write(
                        String.valueOf(data.getNumber1()),
                        String.valueOf(data.getNumber2()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_1, NUMBER_3).toString());
            for (VietLott655Entity data : listDataNumber3) {
                printer.write(
                        String.valueOf(data.getNumber1()),
                        String.valueOf(data.getNumber3()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_1, NUMBER_4).toString());
            for (VietLott655Entity data : listDataNumber4) {
                printer.write(
                        String.valueOf(data.getNumber1()),
                        String.valueOf(data.getNumber4()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_1, NUMBER_5).toString());
            for (VietLott655Entity data : listDataNumber5) {
                printer.write(
                        String.valueOf(data.getNumber1()),
                        String.valueOf(data.getNumber5()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_1, NUMBER_6).toString());
            for (VietLott655Entity data : listDataNumber6) {
                printer.write(
                        String.valueOf(data.getNumber1()),
                        String.valueOf(data.getNumber6()),
                        String.valueOf(data.getCount())
                );
            }

            printer.flush();
            listDataNumber2.clear();
            listDataNumber3.clear();
            listDataNumber4.clear();
            listDataNumber5.clear();
            listDataNumber6.clear();
        }
    }

    private void exportCsvFileByNumber2(List<VietLott655Entity> listDataNumber3,
                                        List<VietLott655Entity> listDataNumber4,
                                        List<VietLott655Entity> listDataNumber5,
                                        List<VietLott655Entity> listDataNumber6) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(
                ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_2))) {
            printer.write(ExportDataUtil.buildHeader(NUMBER_2, NUMBER_3).toString());
            for (VietLott655Entity data : listDataNumber3) {
                printer.write(
                        String.valueOf(data.getNumber2()),
                        String.valueOf(data.getNumber3()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_2, NUMBER_4).toString());
            for (VietLott655Entity data : listDataNumber4) {
                printer.write(
                        String.valueOf(data.getNumber2()),
                        String.valueOf(data.getNumber4()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_2, NUMBER_5).toString());
            for (VietLott655Entity data : listDataNumber5) {
                printer.write(
                        String.valueOf(data.getNumber2()),
                        String.valueOf(data.getNumber5()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_2, NUMBER_6).toString());
            for (VietLott655Entity data : listDataNumber6) {
                printer.write(
                        String.valueOf(data.getNumber2()),
                        String.valueOf(data.getNumber6()),
                        String.valueOf(data.getCount())
                );
            }

            printer.flush();
            listDataNumber3.clear();
            listDataNumber4.clear();
            listDataNumber5.clear();
            listDataNumber6.clear();
        }
    }

    private void exportCsvFileByNumber3(List<VietLott655Entity> listDataNumber4,
                                        List<VietLott655Entity> listDataNumber5,
                                        List<VietLott655Entity> listDataNumber6) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter
                (ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_3))) {
            printer.write(ExportDataUtil.buildHeader(NUMBER_3, NUMBER_4).toString());
            for (VietLott655Entity data : listDataNumber4) {
                printer.write(
                        String.valueOf(data.getNumber3()),
                        String.valueOf(data.getNumber4()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_3, NUMBER_5).toString());
            for (VietLott655Entity data : listDataNumber5) {
                printer.write(
                        String.valueOf(data.getNumber3()),
                        String.valueOf(data.getNumber5()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_3, NUMBER_6).toString());
            for (VietLott655Entity data : listDataNumber6) {
                printer.write(
                        String.valueOf(data.getNumber3()),
                        String.valueOf(data.getNumber6()),
                        String.valueOf(data.getCount())
                );
            }

            printer.flush();
            listDataNumber4.clear();
            listDataNumber5.clear();
            listDataNumber6.clear();
        }
    }


    private void exportCsvFileByNumber4(List<VietLott655Entity> listDataNumber5,
                                        List<VietLott655Entity> listDataNumber6) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(
                ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_4))) {
            printer.write(ExportDataUtil.buildHeader(NUMBER_4, NUMBER_5).toString());
            for (VietLott655Entity data : listDataNumber5) {
                printer.write(
                        String.valueOf(data.getNumber4()),
                        String.valueOf(data.getNumber5()),
                        String.valueOf(data.getCount())
                );
            }
            printer.write(ExportDataUtil.buildHeader(NUMBER_4, NUMBER_6).toString());
            for (VietLott655Entity data : listDataNumber6) {
                printer.write(
                        String.valueOf(data.getNumber4()),
                        String.valueOf(data.getNumber6()),
                        String.valueOf(data.getCount())
                );
            }

            printer.flush();
            listDataNumber5.clear();
            listDataNumber6.clear();
        }
    }

    private void exportCsvFileByNumber5(List<VietLott655Entity> listDataNumber6) {
        try (SimpleCsvPrinter printer = new SimpleCsvPrinter(
                ExportDataUtil.getCsvFileName(CSV_EXPORT_FILE_NAME_6_55_BY_NUMBER_5))) {
            printer.write(ExportDataUtil.buildHeader(NUMBER_5, NUMBER_6).toString());
            for (VietLott655Entity data : listDataNumber6) {
                printer.write(
                        String.valueOf(data.getNumber5()),
                        String.valueOf(data.getNumber6()),
                        String.valueOf(data.getCount())
                );
            }

            printer.flush();
            listDataNumber6.clear();
        }
    }

}
