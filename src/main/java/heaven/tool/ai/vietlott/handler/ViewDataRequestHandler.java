package heaven.tool.ai.vietlott.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;

//Template method design pattern
public abstract class ViewDataRequestHandler {
    private final Logger logger = LoggerFactory.getLogger(ViewDataRequestHandler.class);

    public final void handle(int number, VietLottType vietLottType, String numberType) {
        try {
            preHandle(number, vietLottType, numberType);
            doHandle(number, vietLottType, numberType);
            postHandle(number, vietLottType, numberType);
        } catch (Exception e) {
            logger.error("handle request: {}, ", number, e);
        }
    }

    protected void preHandle(int number, VietLottType vietLottType, String numberType) {
    }

    protected void postHandle(int number, VietLottType vietLottType, String numberType) {
    }

    protected abstract void doHandle(int number, VietLottType vietLottType, String numberType) throws Exception;
}
