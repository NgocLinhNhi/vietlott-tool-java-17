package heaven.tool.ai.vietlott.handler;

import heaven.tool.ai.vietlott.enums.VietLottType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public class VietLott645ViewDataHandler extends VietLott645ViewDataRequestHandler {

    private final Logger logger = LoggerFactory.getLogger(VietLott645ViewDataHandler.class);

    @Override
    protected void preHandle(int number, VietLottType vietLottType, String numberType) {
        logger.info("Begin export file view Number by Number {} - VietLott_645 !!!", numberType);
    }

    @Override
    protected void doHandle(int number, VietLottType vietLottType, String numberType) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Time  Start export data for view VietLott_645 {} ", numberType);

        //Switch expression java 12
        switch (numberType) {
            case NUMBER_1 -> executeViewDataByNumber1(number, vietLottType);
            case NUMBER_2 -> executeViewDataByNumber2(number, vietLottType);
            case NUMBER_3 -> executeViewDataByNumber3(number, vietLottType);
            case NUMBER_4 -> executeViewDataByNumber4(number, vietLottType);
            case NUMBER_5 -> executeViewDataByNumber5(number, vietLottType);
            case NUMBER_6 ->
                    //Kết quả export 389987 records chỉ trong 3405 ms  = 3.4 s
                    //Nhanh hơn chia limit/offset nhiều nhưng export lên 2tr records có thể chết do out of memory...
                    VietLott645ExportBigData.getInstance().exportBigDataNoLimitOffset(vietLottType);
            case NUMBER_7 ->
                    //Kết quả xử lý export csv file cho 389.987 records trong  19256 ms = 19s => chậm hơn nhiều không chia limit/offset
                    //=> Bởi vì khi chia limit off/set ra lại tốn thêm time gọi kết nối DB và xử lý với mỗi câu lệnh
                    //=> Tuy nhiên khi export data tầm 2-3tr records app sẽ không chết do out of memory...
                    //Chỉ có insert 389.987 records  = tận 10 phút ( mặc dù dùng thread + batch + partition ...)
                    //Đánh index thì insert sẽ chậm hơn tí nữa... nên nhớ ...
                    VietLott645ExportBigData.getInstance().exportBigDataUseLimitOffSet(vietLottType);
        }

        long timeEnd = System.currentTimeMillis();
        logger.info("End time export data for view VietLott_645 !!! ");
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }

    @Override
    protected void postHandle(int number, VietLottType vietLottType, String numberType) {
        logger.info("End export file view Number by {} - VietLott_645 !!!", numberType);
    }


}
