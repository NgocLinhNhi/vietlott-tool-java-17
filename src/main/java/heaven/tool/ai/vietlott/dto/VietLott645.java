package heaven.tool.ai.vietlott.dto;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "VIETLOTT_645")
public class VietLott645 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SEQ_ID", nullable = false, unique = true)
    Long seqId;

    @Column(name = "NUMBER_1", nullable = false)
    int number1;

    @Column(name = "NUMBER_2", nullable = false)
    int number2;

    @Column(name = "NUMBER_3", nullable = false)
    int number3;

    @Column(name = "NUMBER_4", nullable = false)
    int number4;

    @Column(name = "NUMBER_5", nullable = false)
    int number5;

    @Column(name = "NUMBER_6", nullable = false)
    int number6;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_RESULT", nullable = false)
    Date dateOfResult;

    @Column(name = "DATE_INSERT", nullable = false)
    String dateInsert;

}
