package heaven.tool.ai.vietlott.interfaces;

import heaven.tool.ai.vietlott.prototype.VietLottData;
import heaven.tool.ai.vietlott.vietlott_chain_of_repository.*;

import java.util.ArrayList;
import java.util.List;

//Java 17 sealed class
public final class VietLottSimpleChainOfHandlers implements VietLottChainOfHandlers<VietLottHandler> {

    //Dùng List ở đây sẽ đánh đổi mỗi lần tạo request xử lý là tạo mới 1 Object List => chuyển HashMap hay hơn
    //Map phải xác định được step nào trước step nào sau nhé =,=!
    private final List<VietLottHandler> handlers;

    public static VietLottSimpleChainOfHandlers getInstance() {
        //=> add lại request
        //Không check null singleton ở đây nếu không sẽ bị double request
        // => vì là 1 List<Object> Handler # HashMap handler key không trùng nhau được !!
        //if (INSTANCE == null) {
        return new VietLottSimpleChainOfHandlers();
        //}
    }

    private VietLottSimpleChainOfHandlers() {
        this.handlers = new ArrayList<>();
    }

    //Hạn chế clone ra các Chain of handlers cái nào gộp được thì cho hết vào 1 VietLottData
    //=> gộp add handler từng module ở đây
    public void addVietLott645Handlers() {
        addHandler(new VietLott645Step1ValidateRequestHandler());
        addHandler(new VietLott645Step2TruncateData());
    }

    //Đầy là Strategy design pattern add vào 1 hanlder => handle
    public void addVietLott655Handlers() {
        addHandler(new VietLott655Step1ValidateRequestHandler());
        addHandler(new VietLott655Step2TruncateData());
    }

    public void addData645ByDateDiffHandlers() {
        addHandler(new VietLottStep1GetDataNearestByDateDiff());
        addHandler(new VietLottStep2GetDataNearestByDateDiff());
    }

    public void addData655ByDateDiffHandlers() {
        addHandler(new VietLottStep1GetDataNearestByDateDiff());
        addHandler(new VietLottStep2GetDataNearestByDateDiff());
    }

    @Override
    public void addHandler(VietLottHandler handler) {
        this.handlers.add(handler);
    }

    @Override
    public void handle(VietLottData data) throws Exception {
        for (VietLottHandler handler : handlers)
            handler.handle(data);
    }
}
