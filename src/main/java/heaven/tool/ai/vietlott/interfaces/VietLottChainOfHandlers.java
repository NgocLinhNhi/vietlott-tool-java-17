package heaven.tool.ai.vietlott.interfaces;

import heaven.tool.ai.vietlott.prototype.VietLottData;

//java 17 sealed class
sealed public interface VietLottChainOfHandlers<T> permits VietLottSimpleChainOfHandlers {
    //Bride design pattern
    void addHandler(T handler);

    //VietLottData handle 1 class cho nó linh động hơn là chỉ List<VietLott645Entity>
    void handle(VietLottData data) throws Exception;

}
