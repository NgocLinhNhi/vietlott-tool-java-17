package heaven.tool.ai.vietlott.interfaces;

import heaven.tool.ai.vietlott.prototype.VietLottData;

public interface VietLottHandler {
    void handle(VietLottData data) throws Exception;
}
