package heaven.tool.ai.vietlott.excel_import_handle;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.excel_export_handle.ExcelWriteContentFile;
import heaven.tool.ai.vietlott.load_config.PropertiesFileData;
import heaven.tool.ai.vietlott.ulti.excel_apache_poi.ExcelParseFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.ulti.excel_apache_poi.SimpleExcelWriter;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public abstract class ExcelRequestHandler {

    private IVietLottDAO createDbProcessor(VietLottType vietLottType) {
        BusinessManageDaoFactory instance = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory factory = instance.getVietLottByType(vietLottType);
        return factory.createVietLott();
    }

    protected List<VietLott645Entity> getListVietLott645(VietLottType vietLottType) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(vietLottType);
        return dbProcessor.loadAllData(vietLottType);
    }

    void insertVietLott645ByExcelFile(List<VietLott645Entity> listData) throws Exception {
        IVietLottDAO dbProcessor = createDbProcessor(VietLottType.VIETLOTT645);
        dbProcessor.insertDataVietLottByFileExcel(listData);
    }

    List<VietLott645Entity> handleImportExcelFile(String excelFilePath) throws Exception {
        List<VietLott645Entity> listData = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(excelFilePath);
        SimpleExcelWriter excelService = new SimpleExcelWriter(
                getImportResultPath(),
                EXCEL_IMPORT_RESULT_NAME);

        try (Workbook workbook = WorkbookFactory.create(inputStream)) {
            Iterator<Row> rowIterator = ExcelParseFormat.checkTypeOfWorkBook(workbook);
            Boolean validateResult = ExcelImportValidator.getInstance().validateRecords(rowIterator, listData);
            excelService.writeFileExcel(validateResult, workbook);
        }
        return listData;
    }

    protected void exportExcelFile(List<VietLott645Entity> listData) throws Exception {
        SimpleExcelWriter simpleExcelWriter = new SimpleExcelWriter(
                getExportResultPath(),
                EXCEL_EXPORT_FILE_NAME);

        try (XSSFWorkbook workbook = simpleExcelWriter.createXSSFWorkbook()) {
            XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
            simpleExcelWriter.writeHeaderLine(sheet, VIETLOTT_645_HEADER);
            ExcelWriteContentFile.getInstance().writeDataLines(listData, workbook, sheet);
            simpleExcelWriter.writeFileExcel(true, workbook);
        }
    }

    private String getImportResultPath() throws Exception {
        return PropertiesFileData.getInstance().loadProperties().getProperty("file.import.result.path");
    }

    private String getExportResultPath() throws Exception {
        return PropertiesFileData.getInstance().loadProperties().getProperty("file.export.result.path");
    }

}
