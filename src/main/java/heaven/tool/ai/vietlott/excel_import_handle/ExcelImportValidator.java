package heaven.tool.ai.vietlott.excel_import_handle;

import heaven.tool.ai.vietlott.constant.DateConstant;
import heaven.tool.ai.vietlott.ulti.date.DateUtil;
import heaven.tool.ai.vietlott.ulti.excel_apache_poi.ExcelParseFormat;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelImportValidator {
    private static final Logger logger = LoggerFactory.getLogger(ExcelImportValidator.class);

    public static ExcelImportValidator getInstance() {
        return new ExcelImportValidator();
    }

    Boolean validateRecords(Iterator<Row> rowIter, List<VietLott645Entity> listData) {
        try {
            //pass 1 row - header
            int maxIndex = 0;
            int seqIdIndex = 0;
            int number1Index = -1;
            int number2Index = -1;
            int number3Index = -1;
            int number4Index = -1;
            int number5Index = -1;
            int number6Index = -1;
            int dateResultIndex = -1;

            if (rowIter.hasNext()) {
                Row row = rowIter.next();  // skip the header row
                int index = 0;//read header
                for (Cell cell : row) {
                    CellType cellType = cell.getCellType();
                    if (cellType.equals(CellType.STRING)) {
                        String value = cell.getStringCellValue();
                        if (value == null) {
                            continue;
                        }
                        if ("PERIOD".equalsIgnoreCase(value)) {
                            seqIdIndex = index;
                        } else if ("NUMBER_1".equalsIgnoreCase(value)) {
                            number1Index = index;
                        } else if ("NUMBER_2".equalsIgnoreCase(value)) {
                            number2Index = index;
                        } else if ("NUMBER_3".equalsIgnoreCase(value)) {
                            number3Index = index;
                        } else if ("NUMBER_4".equalsIgnoreCase(value)) {
                            number4Index = index;
                        } else if ("NUMBER_5".equalsIgnoreCase(value)) {
                            number5Index = index;
                        } else if ("NUMBER_6".equalsIgnoreCase(value)) {
                            number6Index = index;
                        } else if ("DATE_RESULT".equalsIgnoreCase(value)) {
                            dateResultIndex = index;
                        }
                    }
                    index++;
                }
                //Create cell for report result insert
                Cell cell = row.createCell(index);
                cell.setCellValue("Import Result");
                maxIndex = index;
            }

            boolean isValidRow = true;
            while (rowIter.hasNext()) {
                VietLott645Entity data = new VietLott645Entity();
                List<String> errors = new ArrayList<>();
                Row row = rowIter.next();

                if (ExcelParseFormat.isRowEmpty(row)) {
                    continue;
                }

                isValidRow = validateSeqId(seqIdIndex, row, data, errors, isValidRow);
                isValidRow = validateNumber1(number1Index, row, data, errors, isValidRow);
                isValidRow = validateNumber2(number2Index, row, data, errors, isValidRow);
                isValidRow = validateNumber3(number3Index, row, data, errors, isValidRow);
                isValidRow = validateNumber4(number4Index, row, data, errors, isValidRow);
                isValidRow = validateNumber5(number5Index, row, data, errors, isValidRow);
                isValidRow = validateNumber6(number6Index, row, data, errors, isValidRow);
                isValidRow = validateDateResult(dateResultIndex, row, data, errors, isValidRow);

                createCellErrorResult(row, maxIndex, errors);

                if (isValidRow) listData.add(data);
            }
            return isValidRow;
        } catch (IllegalStateException ex) {
            logger.error("IllegalStateException" + ex);
        }
        return false;
    }

    private void createCellErrorResult(Row row, int maxIndex, List<String> errors) {
        Cell cellResult = row.createCell(maxIndex);
        String error = String.join("", errors);
        cellResult.setCellValue(error);
    }

    private boolean validateSeqId(int seqIdIndex,
                                  Row row,
                                  VietLott645Entity data,
                                  List<String> errors,
                                  boolean isValidRow) {
        if (seqIdIndex >= 0) {
            Cell cell = row.getCell(seqIdIndex);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setPeriod(value);
                }
            } else {
                errors.add("Seq ID is required.");
                isValidRow = false;
            }
        } else {
            errors.add("Thiếu cột Seq ID.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateNumber1(int number1Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number1Index >= 0) {
            Cell cell = row.getCell(number1Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber1(value);
                } else {
                    isValidRow = false;
                    errors.add("Number 1 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 1.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateNumber2(int number2Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number2Index >= 0) {
            Cell cell = row.getCell(number2Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber2(value);
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Number 2 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 2.");
            isValidRow = false;
        }
        return isValidRow;
    }


    private boolean validateNumber3(int number3Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number3Index >= 0) {
            Cell cell = row.getCell(number3Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber3(value);
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Number 3 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 3.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateNumber4(int number4Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number4Index >= 0) {
            Cell cell = row.getCell(number4Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber4(value);
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Number 4 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 4.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateNumber5(int number5Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number5Index >= 0) {
            Cell cell = row.getCell(number5Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber5(value);
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Number 5 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 5.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateNumber6(int number6Index,
                                    Row row,
                                    VietLott645Entity data,
                                    List<String> errors,
                                    boolean isValidRow) {
        if (number6Index >= 0) {
            Cell cell = row.getCell(number6Index);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.NUMERIC)) {
                    int value = (int) cell.getNumericCellValue();
                    data.setNumber6(value);
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Number 6 phải là số.");
                }
            }
        } else {
            errors.add("Thiếu cột Number 6.");
            isValidRow = false;
        }
        return isValidRow;
    }

    private boolean validateDateResult(int dateResultIndex,
                                       Row row,
                                       VietLott645Entity data,
                                       List<String> errors,
                                       boolean isValidRow) {
        if (dateResultIndex >= 0) {
            Cell cell = row.getCell(dateResultIndex);
            if (cell != null) {
                CellType cellType = cell.getCellType();
                if (cellType.equals(CellType.STRING)) {
                    String value = cell.getStringCellValue();
                    data.setDateOfResult(DateUtil.toDate(value, DateConstant.PATTERN_YYMMDD_BLANK));
                    isValidRow = true;
                } else {
                    isValidRow = false;
                    errors.add("Date result phải là chuỗi.");
                }
            }
        } else {
            errors.add("Thiếu cột Date result.");
            isValidRow = false;
        }
        return isValidRow;
    }
}
