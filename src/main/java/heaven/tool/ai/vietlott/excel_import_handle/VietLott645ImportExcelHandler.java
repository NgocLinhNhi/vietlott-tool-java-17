package heaven.tool.ai.vietlott.excel_import_handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.methods.BusinessManageDaoFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;

public class VietLott645ImportExcelHandler extends ExcelRequestHandler {

    private final Logger logger = LoggerFactory.getLogger(VietLott645ImportExcelHandler.class);

    public static VietLott645ImportExcelHandler getInstance() {
        return new VietLott645ImportExcelHandler();
    }

    private void preHandle() throws Exception {
        logger.info("start truncate data before import VietLott 6/45 by Excel ");
        BusinessManageDaoFactory instance = BusinessManageDaoFactory.getInstance();
        VietLottAbstractFactory factory = instance.getVietLottByType(VietLottType.VIETLOTT645);
        factory.createVietLott().truncateData();
        logger.info("End truncate data before import VietLott 6/45 by Excel ");
    }

    public void doHandle(String filePath) throws Exception {
        preHandle();
        logger.info("Start Insert data for VietLott645 by Excel file  !!! {}", filePath);
        List<VietLott645Entity> listData = handleImportExcelFile(filePath);
        insertVietLott645ByExcelFile(listData);
        logger.info("List data insert {}", listData.size());
        logger.info("Insert data for VietLott645 by Excel file has successfully !!!");
    }

}
