package heaven.tool.ai.vietlott.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class VietLott655Entity implements Serializable {
    int number1;
    int number2;
    int number3;
    int number4;
    int number5;
    int number6;
    Date dateOfResult;
    int period;

    int count;

    public VietLott655Entity(VietLott655Entity obj) {
        this.number1 = obj.getNumber1();
        this.number2 = obj.getNumber2();
        this.number3 = obj.getNumber3();
        this.number4 = obj.getNumber4();
        this.number5 = obj.getNumber5();
        this.number6 = obj.getNumber6();
        this.dateOfResult = obj.getDateOfResult();
        this.period = obj.getPeriod();
        this.count = obj.getCount();
    }

    public VietLott655Entity() {
    }

    public VietLott655Entity setNumber1(int number1) {
        this.number1 = number1;
        return this;
    }

    public VietLott655Entity setNumber2(int number2) {
        this.number2 = number2;
        return this;
    }

    public VietLott655Entity setNumber3(int number3) {
        this.number3 = number3;
        return this;
    }

    public VietLott655Entity setNumber4(int number4) {
        this.number4 = number4;
        return this;
    }

    public VietLott655Entity setNumber5(int number5) {
        this.number5 = number5;
        return this;
    }

    public VietLott655Entity setNumber6(int number6) {
        this.number6 = number6;
        return this;
    }

    public VietLott655Entity setDateOfResult(Date dateOfResult) {
        this.dateOfResult = dateOfResult;
        return this;
    }

    public VietLott655Entity setPeriod(int period) {
        this.period = period;
        return this;
    }

    public VietLott655Entity setCount(int count) {
        this.count = count;
        return this;
    }

    //không dùng Getter/Setter -> dùng builder design pattern => build từng cell cho linh hoạt !!!
    public VietLott655Entity build() {
        return new VietLott655Entity(this);
    }


}
