package heaven.tool.ai.vietlott.startup;

import heaven.tool.ai.vietlott.factory.methods.DataBaseHandleDaoFactory;
import heaven.tool.ai.vietlott.factory.methods.VietLottResultFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.excel_export_handle.VietLott645ExportExcelHandler;
import heaven.tool.ai.vietlott.excel_import_handle.VietLott645ImportExcelHandler;
import heaven.tool.ai.vietlott.handler.ViewDataRequestHandler;
import heaven.tool.ai.vietlott.load_config.PropertiesFileData;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.prototype.VietLottData;
import heaven.tool.ai.vietlott.vietlott_chain_of_repository.VietLottSelectChainByServiceType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public class VietLottToolMenu {

    private static VietLottToolMenu INSTANCE;

    public static VietLottToolMenu getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottToolMenu();
        return INSTANCE;
    }

    private List<VietLott645Entity> readFileCsv645Data() throws Exception {
        return PropertiesFileData.getInstance().getListData645FromCsvFile();
    }

    private List<VietLott655Entity> readFileCsv655Data() throws Exception {
        return PropertiesFileData.getInstance().getListData655FromCsvFile();
    }

    //Import Excel from Folder on server = APACHE POI
    protected void importExcel() throws Exception {
        String filePathExcel = PropertiesFileData.getInstance().getFilePathExcelVietLott645();
        VietLott645ImportExcelHandler.getInstance().doHandle(filePathExcel);
    }

    //Export Excel from Database and save file on server = APACHE POI
    protected void exportExcel() throws Exception {
        VietLott645ExportExcelHandler.getInstance().handleExportExcelFile(VietLottType.VIETLOTT645);
    }

    @SuppressWarnings("unchecked")
    //Import Csv from folder
    protected void insertVietLottDataProcess(VietLottType vietLottType) throws Exception {
        DataBaseHandleDaoFactory factory = DataBaseHandleDaoFactory.getInstance();
        //Java 10 var type
        var handlers = factory.getVietLottByType(vietLottType);
        handlers.handle(vietLottType == VietLottType.VIETLOTT645 ? readFileCsv645Data() : readFileCsv655Data());
    }

    //View data by CSV file
    protected void exportViewDataByNumber(int number, VietLottType vietLottType, String numberType) {
        VietLottResultFactory factory = VietLottResultFactory.getInstance();
        ViewDataRequestHandler handlers = factory.getVietLottByType(vietLottType);
        handlers.handle(number, vietLottType, numberType);
    }

    //Export file Csv with Big Data Handler
    protected void exportBigData(int number, String numberType) {
        VietLottResultFactory factory = VietLottResultFactory.getInstance();
        ViewDataRequestHandler handlers = factory.getVietLottByType(VietLottType.VIETLOTT645);
        handlers.handle(number, VietLottType.VIETLOTT645, numberType);
    }

    //View data in number days by Csv file
    protected void getDataIn30DaysNearest(VietLottType vietLottType, int dateDiff) throws Exception {
        //Chains of repository design pattern không lồng với template method pattern
        VietLottSelectChainByServiceType instance = VietLottSelectChainByServiceType.getInstance();
        var handlers = instance.getDataByDateDiffChainOfHandlers(vietLottType);
        VietLottData vietLottData = instance.newVietLottDataByDateDiff(vietLottType, dateDiff);
        handlers.handle(vietLottData);
    }

    protected void takeDataByNumber5() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input number 5");
        int number_5 = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            exportViewDataByNumber(number_5, VietLottType.VIETLOTT645, NUMBER_5);
        } else {
            exportViewDataByNumber(number_5, VietLottType.VIETLOTT655, NUMBER_5);
        }
    }

    protected void takeDataByNumber4() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input number 4");
        int number_4 = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            exportViewDataByNumber(number_4, VietLottType.VIETLOTT645, NUMBER_4);
        } else {
            exportViewDataByNumber(number_4, VietLottType.VIETLOTT655, NUMBER_4);
        }
    }

    protected void takeDataByNumber3() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input number 3");
        int number_3 = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            exportViewDataByNumber(number_3, VietLottType.VIETLOTT645, NUMBER_3);
        } else {
            exportViewDataByNumber(number_3, VietLottType.VIETLOTT655, NUMBER_3);
        }
    }

    protected void takeDatabyNumber2() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input number 2");
        int number_2 = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            exportViewDataByNumber(number_2, VietLottType.VIETLOTT645, NUMBER_2);
        } else {
            exportViewDataByNumber(number_2, VietLottType.VIETLOTT655, NUMBER_2);
        }
    }

    protected void takeDataByNumber1() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input number 1");
        int number_1 = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            exportViewDataByNumber(number_1, VietLottType.VIETLOTT645, NUMBER_1);
        } else {
            exportViewDataByNumber(number_1, VietLottType.VIETLOTT655, NUMBER_1);
        }
    }

    protected void takeDataByDateDiff() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input Date Diff ");
        int dateDiff = Integer.parseInt(br.readLine());
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            getDataIn30DaysNearest(VietLottType.VIETLOTT645, dateDiff);
        } else {
            getDataIn30DaysNearest(VietLottType.VIETLOTT655, dateDiff);
        }
    }

    protected void insertVietLottData() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please input VietLott Type : 1 = 6/45 else = 6/55");
        int vietLottType = Integer.parseInt(br.readLine());
        if (vietLottType == 1) {
            getInstance().insertVietLottDataProcess(VietLottType.VIETLOTT645);
        } else {
            getInstance().insertVietLottDataProcess(VietLottType.VIETLOTT655);
        }
    }


}

