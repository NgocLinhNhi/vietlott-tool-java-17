package heaven.tool.ai.vietlott.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static heaven.tool.ai.vietlott.constant.Constant.NUMBER_6;
import static heaven.tool.ai.vietlott.constant.Constant.NUMBER_7;

public class VietLottToolStartUp {
    private static final Logger logger = LoggerFactory.getLogger(VietLottToolStartUp.class);

    private static VietLottToolStartUp INSTANCE;

    public static VietLottToolStartUp getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottToolStartUp();
        return INSTANCE;
    }

    private static int getMenu() {
        int choose = 0;
        try {
            System.out.println("============MENU=========");
            System.out.println("1.insert Data for VietLott 6/45 or 6/55");
            System.out.println("2.Lấy kết quả 6 cặp số Vietlott 6/45 or 6/55 được chọn nhiều nhất trong ? ngày gần nhất");
            System.out.println("3.lấy cặp số VietLott 6/45 or 6/55 từ 2->6 được chọn nhiều nhất với số thứ 1 bạn muốn ");
            System.out.println("4.lấy cặp số VietLott 6/45 or 6/55 từ 3->6 được chọn nhiều nhất với số thứ 2 bạn muốn ");
            System.out.println("5.lấy cặp số VietLott 6/45 or 6/55 từ 4->6 được chọn nhiều nhất với số thứ 3 bạn muốn ");
            System.out.println("6.lấy cặp số VietLott 6/45 or 6/55 từ 5->6 được chọn nhiều nhất với số thứ 4 bạn muốn ");
            System.out.println("7.lấy cặp số VietLott 6/45or 6/55 thứ 6 được chọn nhiều nhất với số thứ 5 bạn muốn ");
            System.out.println("====================================================================== ");
            System.out.println("8.Export Big data (700k-1tr) - No use limit offset ");
            System.out.println("9.Export Big data (700k-1tr) - Use limit offset ");
            System.out.println("10.Import VietLott-645 by excel file (APACHE_POI) ");
            System.out.println("11.Export VietLott-645 to excel file (APACHE_POI) ");
            System.out.println("12.Exit");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Please input your choose: ");
            choose = Integer.parseInt(br.readLine());
        } catch (Exception e) {
            System.out.println("Please input number: ");
        }

        return choose;
    }

    public void start() {
        int menu = 0;
        do {
            try {
                menu = getMenu();
                VietLottToolMenu instance = VietLottToolMenu.getInstance();
                //Switch Expression
                switch (menu) {
                    case 1 -> instance.insertVietLottData();
                    case 2 -> instance.takeDataByDateDiff();
                    case 3 -> instance.takeDataByNumber1();
                    case 4 -> instance.takeDatabyNumber2();
                    case 5 -> instance.takeDataByNumber3();
                    case 6 -> instance.takeDataByNumber4();
                    case 7 -> instance.takeDataByNumber5();
                    case 8 -> instance.exportBigData(1, NUMBER_6);
                    case 9 -> instance.exportBigData(2, NUMBER_7);
                    //Import file Excel by APACHE POI dùng HSSFWorkbook / XSSFWorkbook
                    case 10 -> instance.importExcel();
                    //Export file Excel by APACHE POI dùng HSSFWorkbook / XSSFWorkbook
                    case 11 -> instance.exportExcel();
                    case 12 -> System.exit(0);
                }
            } catch (Exception e) {
                logger.info("Exception {}", e.getMessage());
            }
        } while (menu != 13);
    }

    public static void main(String[] args) {
        //Note : hoặc chạy mục 3 rồi từ số 1 lấy luôn 6 cặp số cũng đc
        getInstance().start();
    }
}
