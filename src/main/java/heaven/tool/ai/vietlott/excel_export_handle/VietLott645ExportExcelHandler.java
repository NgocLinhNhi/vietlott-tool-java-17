package heaven.tool.ai.vietlott.excel_export_handle;

import heaven.tool.ai.vietlott.excel_import_handle.ExcelRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;

import java.util.List;

public class VietLott645ExportExcelHandler extends ExcelRequestHandler {

    private final Logger logger = LoggerFactory.getLogger(VietLott645ExportExcelHandler.class);

    public static VietLott645ExportExcelHandler getInstance() {
        return new VietLott645ExportExcelHandler();
    }

    public void handleExportExcelFile(VietLottType vietLottType) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Start export data to excel file process !!! ");
        List<VietLott645Entity> listData = getListVietLott645(vietLottType);
        logger.info("List Data {} records", listData.size());
        exportExcelFile(listData);
        long timeEnd = System.currentTimeMillis();
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
        logger.info("End export Excel file process !!!");
    }

}
