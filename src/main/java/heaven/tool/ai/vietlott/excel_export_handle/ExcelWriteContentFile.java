package heaven.tool.ai.vietlott.excel_export_handle;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.ulti.excel_apache_poi.SimpleExcelWriter;

import java.util.Date;
import java.util.List;

public class ExcelWriteContentFile {

    public static ExcelWriteContentFile getInstance() {
        return new ExcelWriteContentFile();
    }

    public void writeDataLines(List<VietLott645Entity> listData,
                        XSSFWorkbook workbook,
                        XSSFSheet sheet) {
        int rowCount = 1; // tạo row data đầu tiên

        for (VietLott645Entity data : listData) {
            int period = data.getPeriod();
            int number1 = data.getNumber1();
            int number2 = data.getNumber2();
            int number3 = data.getNumber3();
            int number4 = data.getNumber4();
            int number5 = data.getNumber5();
            int number6 = data.getNumber6();
            Date dateResult = data.getDateOfResult();

            Row row = sheet.createRow(rowCount++);

            int columnCount = 0;
            Cell cell = row.createCell(columnCount++);
            cell.setCellValue(period);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number1);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number2);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number3);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number4);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number5);

            cell = row.createCell(columnCount++);
            cell.setCellValue(number6);

            cell = row.createCell(columnCount++);
            cell.setCellValue(dateResult);
            SimpleExcelWriter.formatDateCell(workbook, cell);
        }
    }

}
