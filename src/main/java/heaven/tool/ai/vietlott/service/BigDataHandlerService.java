package heaven.tool.ai.vietlott.service;

import heaven.tool.ai.vietlott.dao.BatchHandleDao;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.prototype.DataBaseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.List;

//Command design pattern
public class BigDataHandlerService {
    private final Logger logger = LoggerFactory.getLogger(BigDataHandlerService.class);

    private final VietLottType vietLottType;
    public BigDataHandlerService(VietLottType vietLottType) {
        this.vietLottType = vietLottType;
    }

    void truncateDataCommand() throws Exception {
        BatchHandleDao.getInstance().truncateDataBase(vietLottType);
    }

    void insertDataByFileExcel(List<VietLott645Entity> listData) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Time Start for handle insert data VietLott 6/45 By File Excel: {}", timeStart);

        int batchSize = DataBaseUtil.getBatchSize(listData);
        BatchHandleDao.getInstance().insertBatch(
                listData,
                batchSize,
                DataBaseUtil.getTableName(vietLottType));

        long timeEnd = System.currentTimeMillis();
        logger.info("Time End for handle Insert Data By File Excel: {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }

    void insertDataCommand655(List<VietLott655Entity> listData) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Time Start for handle insert data VietLott 6/55: {}", timeStart);

        List<VietLott645Entity> vietLott645Clones = DataBaseUtil.cloneVietLott655(listData);
        int batchSize = DataBaseUtil.getBatchSize(vietLott645Clones);
        BatchHandleDao.getInstance().insertThreadBatchPattern(
                vietLott645Clones,
                batchSize,
                DataBaseUtil.getTableName(vietLottType));

        long timeEnd = System.currentTimeMillis();
        logger.info("Time End for handle Insert Data VietLott 655: {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }

    void insertDataCommand645(List<VietLott645Entity> listData) throws Exception {
        long timeStart = System.currentTimeMillis();
        logger.info("Time Start for handle insert data VietLott 6/45: {}", timeStart);

        int batchSize = DataBaseUtil.getBatchSize(listData);

        //C1: insert bình thường
        // === Result : (100 records trong 2.7s) => 1000 records in 20370ms
        //BatchHandleDao.getInstance().insert(listData, DataBaseUtil.getTableName(vietLottType));

        //C2: Insert = Batch
        // === Result: (100 records trong 1.1s) => 1000 records in 10444ms (10s)
        // (nhanh gấp đôi insert không batch) BATCH_SZIE = 1000 hay 500 hay 200 tốc độ như nhau
        //INSERT 30k records in 312916 ms = 312s = 5.2 phút
//                BatchHandleDao.getInstance().insertBatch(
//                listData,
//                batchSize,
//                DataBaseUtil.getTableName(vietLottType));

        //C3: insert = Chia Partition + Multithreading (Executor) + Batch
        // => 1000 Records in 9s => nếu set up batch_size = 5000 / BatchSize = 500
        // => 1000 records in 3s / BatchSize = 200 => 1k records = 2.7s
        //BatchHandleDao.getInstance().insertExecutorBatch(listData, batchSize, DataBaseUtil.getTableName(vietLottType));

        /*
            Case 1 : (3 Thread): INSERT 30k records trong 129318 ms = 129s = 2 phút 9s ( nhanh gấp 2.5 lần chỉ insertBatch)
            => thông tin là batchSize = 10k => 30k records = chia chỉ 3 thread

            Case 2 : (6 Thread): INSERT 30k records trong 68454 ms = 68s = hơn 1 phút ( nhanh gấp 2 lần 3 thread)
            => chia 5k records batchSize => chia 6 thread (30k records) => nhanh gấp 5 lần insertBatch(10-15 lần insert normal)
            => Nên tận dụng max Thread có thể ( với cấu hình server => để đạt tốt độ insert nhanh nhất) !

            Case 3 : (10 thread ) insert 100k records trong 146990 ms = 2 phút 26s ( nhanh hơn insert Batch trên stack overflow tầm 30s)
            => A. chia 5k records 1 hit vào DB => BatchSize for partition = 10k records thì chia 1 thread
            => B. chia 10k records 1 hit vào DB => BatchSize for partition = 10k records thì chia 1 thread => xong trong 140734 ms (SAME A)

            Case 4 : (10 thread ) insert 389.987 records trong 580664 ms = 580s = gần 10 phút ... (good)
            => chia 5k records 1 hit vào DB => BatchSize for partition = 10k records thì chia 1 thread
            => Muốn nhanh hơn nữa làm 16 cái thread là được.... O___O
        */

        //C4 : use Design pattern kết quả như C3 => code clean
        BatchHandleDao.getInstance().insertThreadBatchPattern(
                listData,
                batchSize,
                DataBaseUtil.getTableName(vietLottType));

        long timeEnd = System.currentTimeMillis();
        logger.info("Time End for handle Insert Data: {}", timeEnd);
        logger.info("Time End - Time Start: {} ms (1s=1000ms)", (timeEnd - timeStart));
    }


}
