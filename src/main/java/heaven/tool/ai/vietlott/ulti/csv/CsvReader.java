package heaven.tool.ai.vietlott.ulti.csv;

import java.util.List;
import java.util.function.Function;

public interface CsvReader {

    int READ_MODE_STREAM = 1;
    int READ_MODE_FOR_EACH = 2;

    List<String[]> read();

    <T> List<T> readToObjects(Function<String[], T> mapper);

    default <T> T readToObject(Function<String[], T> mapper) {
        List<T> objects = readToObjects(mapper);
        if (objects.isEmpty())
            return null;
        return objects.get(0);
    }

}
