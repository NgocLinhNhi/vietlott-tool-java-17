package heaven.tool.ai.vietlott.ulti.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public final class InputStreams {

    private static final AnywayInputStreamFetcher
            INPUT_STREAM_FETCHER = new AnywayInputStreamFetcher();

    private InputStreams() {}

    public static InputStream getInputStream(String filePath) {
        return INPUT_STREAM_FETCHER.getInputStream(filePath);
    }

    public static void lines(String filePath, Consumer<String> consumer) {
        lines(getInputStream(filePath), consumer);
    }

    public static void lines(InputStream inputStream, Consumer<String> consumer) {
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        String line;
        try {
            while ((line = reader.readLine()) != null)
                consumer.accept(line);
        }
        catch (Exception e) {
            throw new IllegalArgumentException("can't read input stream: " + inputStream, e);
        }
    }

}
