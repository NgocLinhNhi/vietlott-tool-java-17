package heaven.tool.ai.vietlott.ulti;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SortCollectionUtil {

    // Sort List VietLott
    public static List<String> sortListVietlott(List<String> listVietLot) {
        listVietLot.sort(new Comparator<String>() {
            public int compare(String o1, String o2) {
                return extractInt(o1) - extractInt(o2);
            }

            int extractInt(String s) {
                String num = s.replaceAll("\\D", "");
                return num.isEmpty() ? 0 : Integer.parseInt(num);
            }
        });
        return listVietLot;
    }

    //Desc
    public static LinkedHashMap<Integer, Integer> sortHashMapDesc(Map<Integer, Integer> unSortedMap) {
        LinkedHashMap<Integer, Integer> reverseSortedMap = new LinkedHashMap<>();

        unSortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> reverseSortedMap.put(x.getKey(), x.getValue()));
        return reverseSortedMap;
    }

    //Asc
    public static LinkedHashMap<Integer, Integer> sortHashMapAsc(Map<Integer, Integer> unSortedMap) {
        LinkedHashMap<Integer, Integer> SortedMap = new LinkedHashMap<>();

        unSortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> SortedMap.put(x.getKey(), x.getValue()));
        return SortedMap;
    }
}
