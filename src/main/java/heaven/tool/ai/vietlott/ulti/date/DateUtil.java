package heaven.tool.ai.vietlott.ulti.date;

import heaven.tool.ai.vietlott.constant.DateConstant;
import heaven.tool.ai.vietlott.ulti.StringUtil;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static heaven.tool.ai.vietlott.constant.DateConstant.*;

public class DateUtil {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private static final ISOChronology CHRONOLOGY = ISOChronology.getInstance();

    protected final static java.time.format.DateTimeFormatter FORMATTER
            = java.time.format.DateTimeFormatter.ofPattern(DateConstant.PATTERN_HHmmSSS);

    public static String parseTimeToString(long millis, String pattern) {
        return toString(new Date(millis), pattern);
    }

    /**
     * Parse Date to String
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String toString(Date date, String pattern) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    /**
     * Parse Calendar to String
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String toString(Calendar date, String pattern) {
        if (date == null) {
            return "";
        }
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date.getTime());
    }

    /**
     * This method use to compare between dateFrom and dateTo
     *
     * @param dateFrom
     * @param dateTo
     * @param pattern
     * @return
     * @throws ParseException
     */
    public static int compare(String dateFrom, String dateTo, String pattern)
            throws ParseException {
        Date dFrom = toDate(dateFrom, pattern);
        Date dTo = toDate(dateTo, pattern);
        if (dFrom != null && dTo != null) {
            long t1 = dFrom.getTime();
            long t2 = dTo.getTime();

            if (t1 - t2 > 0) {
                return 1;
            } else if (t1 == t2) {
                return 0;
            } else
                return -1;
        } else {
            throw new ParseException(pattern, 0);
        }
    }

    /**
     * parse DateTime to Date
     *
     * @param datetime
     * @param pattern
     * @return
     */
    public static Date toDate(String datetime, String pattern) {
        Date date = null;
        if (!StringUtil.isEmpty(datetime)) {
            try {
                DateFormat formatter = new SimpleDateFormat(pattern);
                formatter.setLenient(false);
                date = formatter.parse(datetime);
            } catch (ParseException e) {
                logger.error("Invalid format {}", datetime);
            }
        }
        return date;
    }

    /**
     * Check valid of date input
     *
     * @param dateTime
     * @param pattern
     * @return
     */
    public static boolean checkValidDateTimeFormat(String dateTime, String pattern) {
        Date date = toDate(dateTime, pattern);
        return date != null;
    }

    /**
     * chèn separator vào giữa String date 20200516 -> 2020/05/16
     *
     * @param date
     * @return
     */
    public static String parseDateString(String date) {
        StringBuilder builder = new StringBuilder();
        String year = date.substring(0, 4);
        builder.append(year);
        builder.append("/");

        String month = date.substring(4, 6);
        int monthValue = Integer.parseInt(month);
        builder.append(monthValue);
        builder.append("/");

        String days = date.substring(6, 8);
        int dayValue = Integer.parseInt(days);
        builder.append(dayValue);
        return builder.toString();
    }

    /**
     * Get Previous month with current month
     *
     * @return
     */
    public static String getPreviousMonthOfYear() {
        StringBuilder previousMonth = new StringBuilder();

        LocalDate now = LocalDate.now();
        LocalDate earlier = now.minusMonths(1);

        int month = earlier.getMonth().getValue();
        StringBuilder monthValue = new StringBuilder();
        if (month < 10) {
            monthValue.append(0).append(month);
        } else {
            monthValue.append(month);
        }
        int year = earlier.getYear();
        previousMonth.append(year).append(monthValue);
        return previousMonth.toString();
    }

    /**
     * Add(minus Date) for currentDate -> return String
     * -> Note : không dùng cal.rol(+ tháng + ngày nhưng không nhảy năm issue BO)
     *
     * @param pattern
     * @return
     */
    public static String getPreviousDateOfCurrentDate(String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -1); //-> Note : không dùng cal.rol(+ tháng + ngày nhưng không nhảy năm issue BO)
        return dateFormat.format(cal.getTime());
    }

    /**
     * Add Date for input Date = String -> return String
     * -> Note : không dùng cal.rol(+ tháng + ngày nhưng không nhảy năm issue BO)
     *
     * @param pattern
     * @param dateInput
     * @return
     * @throws ParseException
     */
    public static String getNextDate(String pattern, String dateInput, int numberDay) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        Calendar cal = Calendar.getInstance();
        DateFormat format = new SimpleDateFormat(PATTERN_YYMMDD_BLANK);
        Date date = format.parse(dateInput);
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, numberDay); //-> Note : không dùng cal.rol(+ tháng + ngày nhưng không nhảy năm issue BO)
        return dateFormat.format(cal.getTime());
    }

    /**
     * Add Day/Month/Year for input Date = Date -> Return Date = ý nghĩa hàm getPreviousDateOfCurrentDate()
     *
     * @param date
     * @param field
     * @param amount
     * @return
     */
    public static Date dateAdd(Date date, int field, int amount) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(field, amount);
        return cal.getTime();
    }


    /**
     * convert dateTime to TimeStamp
     *
     * @param datetime
     * @param pattern
     * @return
     */
    public static Timestamp toTimestamp(String datetime, String pattern) {
        if (StringUtil.isEmpty(datetime)) return null;
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat(pattern);
            formatter.setLenient(false);
            date = formatter.parse(datetime);
        } catch (ParseException e) {
            logger.warn("toTimestamp {}", datetime);
        }
        if (date == null) {
            return null;
        }
        return new Timestamp(date.getTime());
    }

    /**
     * parse dateTime input to calendar
     *
     * @param datetime
     * @param pattern
     * @return
     */
    private static Calendar toCalendar(String datetime, String pattern) {
        if (StringUtil.isEmpty(datetime)) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        Date date = null;
        try {
            DateFormat formatter = new SimpleDateFormat(pattern);
            formatter.setLenient(false);
            date = formatter.parse(datetime);
        } catch (ParseException e) {
            logger.warn("parse date exception {}", (datetime == null ? "null" : datetime));
        }
        if (date == null) {
            return calendar;
        }
        calendar.setTime(date);
        return calendar;
    }

    /**
     * Check is first Day of month of current Date
     *
     * @return
     */
    public static boolean isFirstDayOfTheMonth() {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());

        return c.get(Calendar.DAY_OF_MONTH) == 1;
    }

    /**
     * Check is first Day of Year of current Date
     *
     * @return
     */
    public static boolean isFirstDayOfTheYear() {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());

        return c.get(Calendar.DAY_OF_YEAR) == 1;
    }

    /**
     * Check first day of year by date input
     *
     * @param frontDate
     * @param firstDateOfYear
     * @return
     */
    public static boolean checkFirstDayOfTheYear(String frontDate, String firstDateOfYear) {
        String firstDateYear = frontDate.substring(4, 8);
        return firstDateYear.equals(firstDateOfYear);
    }

    /**
     * Parse from DateFormat type 1 -> DateFormat type 2
     *
     * @param value
     * @param sourceFormat - Format of source begin
     * @param destFormat   -> Format need change
     * @return
     */
    public static String convertBetweenDateFormat(String value, String sourceFormat, String destFormat) {
        if (!StringUtil.isEmpty(value)) {
            try {
                SimpleDateFormat sdfSource = new SimpleDateFormat(sourceFormat);
                Date date = sdfSource.parse(value);
                SimpleDateFormat sdfDestination = new SimpleDateFormat(destFormat);
                value = sdfDestination.format(date);
                return value;
            } catch (Exception e) {
                logger.warn("error: ", e);
            }
        }
        return null;
    }

    /**
     * Check is valid of inputDate by pattern
     *
     * @param inDate
     * @param pattern
     * @return
     */
    public static boolean isValidDate(String inDate, String pattern) {

        if (inDate == null) return false;
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        if (inDate.trim().length() != dateFormat.toPattern().length()) return false;
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }


    /**
     * Add number of days for fromDate input
     *
     * @param fromDate
     * @param days
     * @param pattern
     * @return
     */
    public static String getOtherDateFromDate(String fromDate, int days, String pattern) {
        String targetDate = "";
        Calendar calendar = toCalendar(fromDate, pattern);
        if (calendar == null) {
            return targetDate;
        }
        calendar.add(Calendar.DAY_OF_MONTH, days);
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            targetDate = formatter.format(calendar.getTime());
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return targetDate;
    }

    public static String toHourFormat(String input) {
        String[] array = input.split("(?<=\\G.{2})");
        return String.join(":", array);
    }

    public static String roundMinute(String date) {
        DateTimeFormatter df = DateTimeFormat.forPattern(PATTERN_YYYMMDD_HHMMSS);
        return roundMinute(df.parseDateTime(date), df);
    }

    public static String roundMinute(long timeInMillis, String pattern) {
        DateTimeFormatter df = DateTimeFormat.forPattern(pattern);
        return roundMinute(new DateTime(timeInMillis), df);
    }

    public static String roundMinute(DateTime date, DateTimeFormatter df) {
        DateTime update = date.minus(date.getSecondOfMinute() * 1000L + date.getMillisOfSecond());
        return df.print(update);
    }

    public static Date getStartDateOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getEndDateOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static String getTime(LocalTime time) {
        return FORMATTER.format(time);
    }

    /**
     * Get Diff time between date1 and date2
     *
     * @param date1
     * @param date2
     * @return
     */
    public static long getDiffTimeInSecond(DateTime date1, DateTime date2) {
        Seconds seconds = Seconds.secondsBetween(date1, date2);
        return seconds.getSeconds();
    }

    /**
     * get diff time of date input and String input
     *
     * @param date1
     * @param date2String
     * @param pattern
     * @return
     */
    public static long getDiffTimeInSecond(DateTime date1, String date2String, String pattern) {
        DateTimeFormatter sdf = DateTimeFormat.forPattern(pattern);
        DateTime date = sdf.parseDateTime(date1.toString(pattern));
        DateTime date2 = sdf.parseDateTime(date2String);
        return getDiffTimeInSecond(date, date2);
    }

    /**
     * get diff time of string input
     *
     * @param date1String
     * @param date2String
     * @param pattern
     * @return
     */
    public static long getDiffTimeInSecond(String date1String, String date2String, String pattern) {
        DateTimeFormatter sdf = DateTimeFormat.forPattern(pattern);
        DateTime date1 = sdf.parseDateTime(date1String);
        DateTime date2 = sdf.parseDateTime(date2String);
        return getDiffTimeInSecond(date1, date2);
    }

    public static String getFirstDayOfTheWeekString() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        Date firstDayOfTheWeek = cal.getTime();
        return toString(firstDayOfTheWeek, FORMAT_USER_DATETIME);
    }

    public static String getLastDayOfTheWeekString() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
        Date lastDayOfTheWeek = cal.getTime();
        return toString(lastDayOfTheWeek, FORMAT_USER_DATETIME);
    }

    public static Date getFirstDayOfTheWeek() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return cal.getTime();
    }

    public static Date getLastDayOfTheWeek() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        return cal.getTime();
    }

    /**
     * check ngày hiện tại có phải là cuối tuần không
     * để làm gì đấy -> chẳng hạn clear cache vào thứ 7
     */
    public static void checkDayOfWeek() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        if (dayOfWeek.compareTo(DayOfWeek.SATURDAY) < 0)
            return;
    }

    public static Date getFirstDayOfQuarter(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(2, cal.get(2) / 3 * 3);
        cal.set(5, 1);
        return cal.getTime();
    }

    public static Date getLastDayOfQuarter(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(2, cal.get(2) / 3 * 3 + 2);
        cal.set(5, cal.getActualMaximum(5));
        return cal.getTime();
    }


    /**
     * convertUtC time to long
     *
     * @param time
     * @param toLocal
     * @return
     */
    public static long convertUtcAndLocal(long time, boolean toLocal) {
        // convert UTC 0 and time zone of server
        int offset = TimeZone.getDefault().getRawOffset();
        int h = offset / 3600000;
        int m = (offset / 60000) % 60;

        if (toLocal == false) { // local server time to UTC 0
            h = h * (-1);
            m = m * (-1);
        }
        time = DateUtil.addHours(time, h);
        time = DateUtil.addMinutes(time, m);
        return time;
    }

    public static java.sql.Date convertDateUtilToDateSQL(Date date) {
        return new java.sql.Date(date.getTime());
    }


    public static long addHours(long millis, int delta) {
        return CHRONOLOGY.hours().add(millis, delta);
    }

    public static long addYears(long millis, int delta) {
        return CHRONOLOGY.years().add(millis, delta);
    }

    public static long addMonths(long millis, int delta) {
        return CHRONOLOGY.months().add(millis, delta);
    }

    public static long addMinutes(long millis, int delta) {
        return CHRONOLOGY.minutes().add(millis, delta);
    }

    public static long addSeconds(long millis, int delta) {
        return CHRONOLOGY.seconds().add(millis, delta);
    }


    public static void main(String[] args) throws Exception {
        Date date = toDate("2020/05/01", PATTERN_YYMMDD);
        System.out.println("To Date: " + date);

        String previousMonth = getPreviousMonthOfYear();
        System.out.println("Previous month of current time: " + previousMonth);

        String previousDate = getPreviousDateOfCurrentDate(PATTERN_YYMMDD);
        System.out.println("Previous Date Of Current Date: " + previousDate);

        String nextDate = getNextDate(PATTERN_YYMMDD_BLANK, "20201231", 1);
        System.out.println("next date of date input " + nextDate);

        Timestamp timestamp = toTimestamp("2020/05/16 00:00:00", PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS);
        System.out.println("Convert Date time to timeStamp: " + timestamp);

        Calendar calendar = toCalendar("2020/05/16", PATTERN_YYMMDD);
        System.out.println("Parse DateTime to calendar : " + calendar.getTime());

        Date d = dateAdd(new Date(), Calendar.DAY_OF_YEAR, -1);
        System.out.println("Add Day return Date :" + d);

        boolean firstDayOfTheMonth = isFirstDayOfTheMonth();
        System.out.println("Check is first day of month of current Date : " + firstDayOfTheMonth);

        boolean firstDayOfTheYear = isFirstDayOfTheYear();
        System.out.println("Check is first day of Year of current Date : " + firstDayOfTheYear);

        boolean checkFirstDayOfTheYear = checkFirstDayOfTheYear("20210101", "0101");
        System.out.println("Check is first day of year by date input :" + checkFirstDayOfTheYear);

        String dateToString = toString(new Date(), PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS);
        System.out.println("Parse Date to String:  " + dateToString);

        Calendar cal = Calendar.getInstance();
        String calendarToString = toString(cal, PATTERN_YYMMDD);
        System.out.println("Parse Calendar to String:  " + calendarToString);

        String convertDateFormat = convertBetweenDateFormat("2020/01/05", PATTERN_YYMMDD, PATTERN_YYMMDD_BLANK);
        System.out.println("Convert Date Format: " + convertDateFormat);

        boolean validDate = isValidDate("2020/05/16 01:01:0", PATTERN_YYYMMDD_HHMMSS_FULL_SLASH);
        System.out.println("Valid Date : " + validDate);

        String addDateForFromDate = getOtherDateFromDate("2020/05/16", 2, PATTERN_YYMMDD);
        System.out.println("Date days for From Date input: " + addDateForFromDate);

        String toHourFormat = toHourFormat("20200516");
        System.out.println("toHourFormat " + toHourFormat);

        String roundMinute = roundMinute("20200516 05:20:33");
        System.out.println("round second :" + roundMinute);

        String firstDayOfTheWeekString = getFirstDayOfTheWeekString();
        System.out.println("First day of the week : " + firstDayOfTheWeekString);

        Date firstDayOfQuarter = getFirstDayOfQuarter(new Date());
        System.out.println("First day of Quarter: " + firstDayOfQuarter);

        Date lastDayOfQuarter = getLastDayOfQuarter(new Date());
        System.out.println("Last day of quarter: " + lastDayOfQuarter);
    }

}
