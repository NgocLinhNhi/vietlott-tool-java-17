package heaven.tool.ai.vietlott.ulti.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

public class SimpleCsvReader implements CsvReader {

    protected int skip;
    protected int limit;
    protected int readMode;
    protected Path filePath;
    protected Function<String, String[]> splitter;
    protected final static Function<String, String[]> DEFAULT_SPLITTER = l -> l.split(",");

    public SimpleCsvReader() {
        this.limit = Integer.MAX_VALUE; //giới hạn row file csv = giá trị Integer
        this.readMode = READ_MODE_STREAM;
    }

    public SimpleCsvReader skip(int skip) {
        this.skip = skip;
        return this;
    }

    public SimpleCsvReader limit(int limit) {
        this.limit = limit;
        return this;
    }

    public SimpleCsvReader readMode(int readMode) {
        this.readMode = readMode;
        return this;
    }

    public SimpleCsvReader filePath(String filePath) {
        if (filePath == null)
            throw new IllegalArgumentException("file path can not be null");
        this.filePath = Paths.get(filePath);
        return this;
    }

    public SimpleCsvReader filePath(File filePath) {
        this.filePath = filePath.toPath();
        return this;
    }

    public SimpleCsvReader filePath(Path filePath) {
        this.filePath = filePath;
        return this;
    }

    //xử lý chế độ read file csv
    @Override
    public List<String[]> read() {
        if (splitter == null)
            splitter = DEFAULT_SPLITTER;
        List<String[]> result = new ArrayList<>();
        try {
            if (readMode == READ_MODE_STREAM)
                readInStreamMode(result);
            else
                readInForeachMode(result);
        } catch (Exception e) {
            throw new IllegalArgumentException("can't read file: " + filePath, e);
        }
        return result;
    }

    //read File csv và add vào list<String[]>
    protected void readInStreamMode(List<String[]> result) throws Exception {
        AtomicInteger lineCount = new AtomicInteger();
        try (Stream<String> content = Files.lines(filePath)) {
            content.forEach(l -> {
                int lc = lineCount.getAndIncrement();
                if (lc < skip)
                    return;
                if (result.size() >= limit)
                    return;
                result.add(splitter.apply(l));
            });
        }
    }

    protected void readInForeachMode(List<String[]> result) throws Exception {
        FileReader fileReader = new FileReader(filePath.toFile());
        AtomicInteger lineCount = new AtomicInteger();
        try (BufferedReader reader = new BufferedReader(fileReader)) {
            String line;
            while ((line = reader.readLine()) != null) {
                int lc = lineCount.getAndIncrement();
                if (lc < skip) {
                    continue;
                }
                if (result.size() >= limit) {
                    return;
                }
                result.add(splitter.apply(line));
            }
        }
    }

    @Override
    public <T> List<T> readToObjects(Function<String[], T> mapper) {
        List<String[]> data = read();
        List<T> result = new ArrayList<>();
        for (String[] properties : data) {
            T obj = mapper.apply(properties);
            result.add(obj);
        }
        return result;
    }
}
