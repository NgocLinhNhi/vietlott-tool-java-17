package heaven.tool.ai.vietlott.ulti.io;

import java.io.File;
import java.io.IOException;

public final class NFiles {

    private NFiles() {}

    public static File newFile(String fileName) {
        try {
            File file = new File(fileName);
            File parent = file.getParentFile();
            if (parent != null && !parent.exists())
                parent.mkdirs();
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    throw new IOException("File already exists");
                }
            }
            return file;
        }
        catch (Exception e) {
            throw new IllegalArgumentException("can't create new file: " + fileName);
        }
    }

}
