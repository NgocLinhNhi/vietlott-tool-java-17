package heaven.tool.ai.vietlott.ulti.excel_apache_poi;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class ExcelParseFormat {

    private static final Logger logger = LoggerFactory.getLogger(ExcelParseFormat.class);

    public static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != CellType.BLANK) {
                return false;
            }
        }
        return true;
    }

    public static Iterator<Row> checkTypeOfWorkBook(Workbook workbook) {
        Iterator<Row> rowIterator = null;
        // excel 97 format
        if (workbook instanceof HSSFWorkbook) {
            logger.info("Parse HSSFWorkbook");
            rowIterator = ExcelParseFormat.parseBatch97Format(workbook);
            // excel 2007+ format
        } else if (workbook instanceof XSSFWorkbook) {
            logger.info("Parse XSSFWorkbook");
            rowIterator = ExcelParseFormat.parseBatch2007Format(workbook);
            //được sử dụng khi xuất các file excel lớn và có bộ nhớ heap sapce hạn chế.
        } else if (workbook instanceof SXSSFWorkbook) {
            logger.info("Parse SXSSFWorkbook");
            rowIterator = ExcelParseFormat.parseSXSSF(workbook);
        }
        return rowIterator;
    }

    private static Iterator<Row> parseSXSSF(Workbook wb) {
        SXSSFWorkbook myWorkBook;
        try {
            myWorkBook = (SXSSFWorkbook) wb;
            // Get the first sheet from workbook
            SXSSFSheet mySheet = myWorkBook.getSheetAt(0);
            return mySheet.rowIterator();
        } catch (Exception e) {
            logger.error("Parse Excel Exception : {0}", e);
        }
        return null;
    }

    private static Iterator<Row> parseBatch97Format(Workbook wb) {
        HSSFWorkbook myWorkBook;
        try {
            myWorkBook = (HSSFWorkbook) wb;
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            return mySheet.rowIterator();
        } catch (Exception e) {
            logger.error("Parse Excel Exception : {0}", e);
        }
        return null;
    }

    private static Iterator<Row> parseBatch2007Format(Workbook wb) {
        XSSFWorkbook myWorkBook;
        try {
            myWorkBook = (XSSFWorkbook) wb;
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);
            return mySheet.rowIterator();
        } catch (Exception e) {
            logger.error("Parse Excel Exception : {0}", e);
        }
        return null;
    }
}
