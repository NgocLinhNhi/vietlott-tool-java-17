package heaven.tool.ai.vietlott.ulti.csv;

import java.util.Collection;

public interface CsvPrinter {

	void close();

	void flush();

	void write(String line);

	void write(String... values);

	void write(Iterable<String> values);
	
	void write(Collection<String[]> lines);

	void writeLines(Collection<String> lines);
}
