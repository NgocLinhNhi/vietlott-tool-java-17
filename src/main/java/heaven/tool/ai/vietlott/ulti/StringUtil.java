package heaven.tool.ai.vietlott.ulti;

import org.assertj.core.util.Lists;

import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    private static final char[] HEX_ARRAY_LOWERCASE = {
            '0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    private static final String FREE_MARKER_SYNTAX = "<(?=\\/?(#list|#if))";

    public static boolean isEmpty(String str) {
        return (str == null || str.trim().length() == 0);
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isStringNullOrEmpty(Object obj1) {
        return obj1 == null || obj1.toString().trim().equals("");
    }

    public static boolean isListNullOrEmpty(List<?> lst) {
        return lst == null || lst.isEmpty();
    }

    public static boolean isNullOrEmpty(String obj1) {
        return obj1 == null || "".equals(obj1.trim());
    }

    public static String toString(Object obj) {
        if (obj == null)
            return "";
        return obj.toString().trim();
    }

    public static boolean isEmail(String email) {
        if (isEmpty(email))
            return false;
        final String EMAIL_PATTERN = "^[_a-zA-Z0-9-](\\.?([_a-zA-Z0-9-])+)*\\@[a-zA-Z0-9-](\\.?([a-zA-Z0-9-])+)*\\.([a-zA-Z0-9-])+$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String toLowerCase(String text) {
        try {
            return text.toLowerCase().trim();
        } catch (Exception ex) {
            return "";
        }
    }

    public static String toUpperCase(String text) {
        if (text == null)
            return "";
        return text.toUpperCase().trim();
    }

    public static String substring(String string, Integer maxLength) {
        if (string == null)
            return "";
        if (isEmpty(string) || string.length() <= maxLength)
            return string;
        return string.substring(0, maxLength);
    }


    /**
     * check Is Integer Number of String
     *
     * @param value
     * @return
     */
    public static boolean isIntegerNumber(String value) {
        try {
            Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static boolean isLatinh(String text) {
        if (StringUtil.isEmpty(text)) {
            return false;
        }
        char[] cs = text.toCharArray();
        for (char c : cs) {
            if ('\u0000' > c || c > '\u007F') {
                return false;
            }
        }
        return true;
    }

    public static boolean isMaxLength(String text, int max) {
        if (StringUtil.isEmpty(text)) {
            return false;
        }
        return text.codePoints().count() <= max;
    }

    public static String printHex(byte[] bytes) {
        return printHex(bytes, HEX_ARRAY_LOWERCASE);
    }

    public static String printHex(byte[] bytes, char[] hexArray) {
        char[] hexChars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; ++i) {
            int v = bytes[i] & 0xFF;
            hexChars[i * 2] = hexArray[v >>> 4];
            hexChars[i * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String nullToEmpty(String s) {
        return s == null ? "" : s;
    }

    public static String rtrim(String s, int max) {
        if (s == null)
            return null;
        int le = s.length();
        if (le > max)
            s = s.substring(le - max, le);
        return s;
    }

    public static String rangeAppend(String first, String second, int maxRange) {
        StringBuilder builder = new StringBuilder();
        if (StringUtil.isEmpty(second)) {
            return first;
        }
        if (StringUtil.isEmpty(first)) {
            return StringUtil.rtrim(second, maxRange);
        }
        builder.append(first);
        builder.append(", ");
        builder.append(second);
        return StringUtil.rtrim(builder.toString(), maxRange);
    }

    public static boolean isSupportFreeMarker(String input) {
        if (StringUtil.isEmpty(input)) return false;
        Pattern r = Pattern.compile(FREE_MARKER_SYNTAX);
        return r.matcher(input).find();
    }

    /**
     * Convert List to String
     *
     * @param list
     * @return
     */
    public static String toString(List<String> list) {
        if (list == null || list.size() == 0)
            return "";
        StringBuilder strB = new StringBuilder();
        boolean isFirst = true;
        for (String item : list) {
            if (isFirst)
                isFirst = false;
            else
                strB.append(", ");
            strB.append(item);
        }
        return strB.toString();
    }

    /**
     * Check validate String pattern
     *
     * @param value
     * @param pattern
     * @return
     */
    public static boolean validateAgainstPattern(String value, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(value);
        return matcher.matches();
    }

    public static int countChar(String str, char reg) {
        char[] ch = str.toCharArray();
        int count = 0;
        for (int i = 0; i < ch.length; ++i) {
            if (ch[i] == reg) {
                if (ch[i + 1] == reg) {
                    ++i;
                } else {
                    ++count;
                }
            }
        }
        return count;
    }

    public static <T> T getMapValue(Map<String, Object> params, String key, Class<T> type) {
        Object obj = params.get(key);
        if (obj == null) {
            return null;
        } else {
            return obj.getClass().isAssignableFrom(obj.getClass()) ? type.cast(obj) : null;
        }
    }

    public static List cloneList(List lstObj) {
        List lstClone = new ArrayList();
        if (isListNullOrEmpty(lstObj)) {
            return lstClone;
        } else {
            for (Object lstObj1 : lstObj) {
                Object obj = cloneObject(lstObj1);
                if (obj != null) {
                    lstClone.add(obj);
                }
            }

            return lstClone;
        }
    }

    public static Object cloneObject(Object obj) {
        try {
            Object clone = obj.getClass().newInstance();
            Field[] var5;
            int var4 = (var5 = obj.getClass().getDeclaredFields()).length;

            for (int var3 = 0; var3 < var4; ++var3) {
                Field field = var5[var3];
                field.setAccessible(true);
                field.set(clone, field.get(obj));
            }

            return clone;
        } catch (Exception var6) {
            return null;
        }
    }

    public static List<String> splitListFile(String strFiles) {
        List<String> lstFile = Lists.newArrayList();
        if (!isStringNullOrEmpty(strFiles)) {
            String[] lst = strFiles.split(",");
            lstFile = Arrays.asList(lst);
        }
        return lstFile;
    }


    public static int compare(Object a, Object b) {
        if ((a != null || b != null) && (a == null || b == null || a != b)) {
            if (a != null && b != null && a == b) {
                return 0;
            } else if (a == null && b != null) {
                return 1;
            } else if (a != null && b == null) {
                return 1;
            } else {
                return a != null && b != null && a != b ? 1 : 0;
            }
        } else {
            return 0;
        }
    }

    public static int compareString(Object a, Object b) {
        if ((a != null || b != null) && (a == null || b == null || a != b)) {
            if (a != null && b != null && a.equals(b)) {
                return 0;
            } else if (a == null && b != null) {
                return 1;
            } else if (a != null && b == null) {
                return 1;
            } else {
                return a != null && b != null && !a.equals(b) ? 1 : 0;
            }
        } else {
            return 0;
        }
    }

}
