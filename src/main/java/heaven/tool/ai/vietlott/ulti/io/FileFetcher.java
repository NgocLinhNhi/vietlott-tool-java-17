package heaven.tool.ai.vietlott.ulti.io;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
