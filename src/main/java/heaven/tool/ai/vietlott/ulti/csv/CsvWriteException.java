package heaven.tool.ai.vietlott.ulti.csv;

public class CsvWriteException extends RuntimeException {

    public CsvWriteException(String msg, Exception e) {
        super(msg, e);
    }

}
