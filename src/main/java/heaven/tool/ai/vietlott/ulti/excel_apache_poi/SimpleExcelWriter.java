package heaven.tool.ai.vietlott.ulti.excel_apache_poi;

import heaven.tool.ai.vietlott.constant.DateConstant;
import heaven.tool.ai.vietlott.ulti.StringUtil;
import heaven.tool.ai.vietlott.ulti.io.NFiles;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class SimpleExcelWriter implements ISimpleExcelWriter {
    private final Logger logger = LoggerFactory.getLogger(SimpleExcelWriter.class);

    private final String filePath;
    private final String exportFileName;

    public SimpleExcelWriter(String filePath, String exportFileName) {
        this.filePath = filePath;
        this.exportFileName = exportFileName;
    }

    @Override
    public XSSFWorkbook createXSSFWorkbook() {
        return new XSSFWorkbook();
    }

    //SXSSFWorkbook use for export file large data - avoid get java heap space - out of memory
    @Override
    public SXSSFWorkbook createSXSSFWorkbook() {
        return new SXSSFWorkbook();
    }

    //save file on server
    @Override
    public void writeFileExcel(Boolean validateResult, Workbook workbook) throws Exception {
        String filePathExcel = createFilePath();
        NFiles.newFile(filePathExcel);
        try (FileOutputStream outputStream = new FileOutputStream(filePathExcel)) {
            workbook.write(outputStream);
            outputStream.flush();
        }
        if (!validateResult) throw new IllegalArgumentException("Validate file has error please view file detail");
    }

    private String createFilePath() {
        //Để như này thì file kết quả import sẽ lưu vào đây => C:\Users\Computer\AppData\Local\Temp
        //return System.getProperty("java.io.tmpdir") + File.separator + "temp";
        String filePathExcel = filePath + File.separator + exportFileName + getCurrentTimeCreateFile() + ".xlsx";
        logger.info("Export File Path: {}", filePathExcel);
        return filePathExcel;
    }

    private long getCurrentTimeCreateFile() {
        return new Date().getTime();
    }

    // Build Header Excel Utils.
    @Override
    public void writeHeaderLine(XSSFSheet sheet, String headerTitles) {
        createHeader(sheet, parseListTitles(headerTitles));
    }

    private List<String> parseListTitles(String headerTitles) {
        return StringUtil.splitListFile(headerTitles);
    }

    private void createHeader(XSSFSheet sheet, List<String> titles) {
        Row headerRow = sheet.createRow(0);
        AtomicInteger columnCount = new AtomicInteger();
        for (String title : titles) {
            Cell cell = headerRow.createCell(columnCount.getAndIncrement());
            cell.setCellValue(title);
        }
    }

    //Utils handle cellType
    public static void formatDateCell(XSSFWorkbook workbook, Cell cell) {
        CellStyle cellStyle = workbook.createCellStyle();
        CreationHelper creationHelper = workbook.getCreationHelper();
        cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat(DateConstant.PATTERN_YY_MM_DD));
        cell.setCellStyle(cellStyle);
    }
}
