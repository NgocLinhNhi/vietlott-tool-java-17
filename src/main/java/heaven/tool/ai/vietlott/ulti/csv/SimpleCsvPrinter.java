package heaven.tool.ai.vietlott.ulti.csv;

import lombok.Setter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import heaven.tool.ai.vietlott.ulti.io.NFiles;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

public class SimpleCsvPrinter implements CsvPrinter, Closeable {
    //
    @Setter
    protected boolean autoFlush;
    private final CSVPrinter writer;

    public SimpleCsvPrinter(String fileName) {
        this(fileName, false);
    }

    public SimpleCsvPrinter(String fileName, boolean append) {
        try {
            File file = NFiles.newFile(fileName);
            this.writer = new CSVPrinter(new FileWriter(file, append), CSVFormat.DEFAULT);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }

    public SimpleCsvPrinter(Appendable appendable) {
        try {
            this.writer = new CSVPrinter(appendable, CSVFormat.DEFAULT);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }

    public SimpleCsvPrinter(OutputStream stream) {
        this(stream, StandardCharsets.UTF_8);
    }

    public SimpleCsvPrinter(OutputStream stream, Charset charset) {
        this(new OutputStreamWriter(stream, charset));
    }

    @Override
    public void flush() {
        try {
            this.writer.flush();
        } catch (Exception e) {
            throw new CsvWriteException("flush data error", e);
        }
    }

    @Override
    public void close() {
        try {
            this.writer.close();
        } catch (Exception e) {
            throw new CsvWriteException("close csv writer error", e);
        }
    }

    @Override
    public void write(String line) {
        try {
            this.writer.printRecord(line.split(","));
        } catch (Exception e) {
            throw new CsvWriteException("write line: " + line + " error", e);
        }
        this.postWrite();
    }

    @Override
    public void write(String... values) {
        try {
            this.writer.printRecord(values);
        } catch (Exception e) {
            throw new CsvWriteException("write line array error", e);
        }
        this.postWrite();

    }

    @Override
    public void write(Iterable<String> values) {
        try {
            this.writer.printRecord(values);
        } catch (Exception e) {
            throw new CsvWriteException("write line iterable error", e);
        }
        this.postWrite();

    }

    @Override
    public void write(Collection<String[]> lines) {
        try {
            for (String[] arr : lines)
                this.writer.printRecord(arr);
        } catch (Exception e) {
            throw new CsvWriteException("write line collection error", e);
        }
        this.postWrite();
    }

    @Override
    public void writeLines(Collection<String> lines) {
        try {
            for (String line : lines)
                this.writer.printRecord(line.split(","));
        } catch (Exception e) {
            throw new CsvWriteException("write lines collection error", e);
        }
        this.postWrite();
    }

    protected void postWrite() {
        if (autoFlush)
            flush();
    }

    public SimpleCsvPrinter(Appendable appendable, CSVFormat format) {
        try {
            this.writer = new CSVPrinter(appendable, format);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }
}
