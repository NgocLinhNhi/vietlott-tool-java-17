package heaven.tool.ai.vietlott.ulti;

import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;
import heaven.tool.ai.vietlott.ulti.csv.CsvReader;
import heaven.tool.ai.vietlott.ulti.csv.SimpleCsvReader;
import heaven.tool.ai.vietlott.ulti.date.DateUtil;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.DateConstant.PATTERN_YYMMDD_BLANK;

public class CsvReaderUtil {
    private static CsvReaderUtil INSTANCE;

    public static CsvReaderUtil getInstance() {
        if (INSTANCE == null) INSTANCE = new CsvReaderUtil();
        return INSTANCE;
    }

    public List<VietLott645Entity> readDataVietLott645(String filePath) {
        //Builder design pattern
        CsvReader csvReader = new SimpleCsvReader()
                //bỏ line đầu -> line tiêu đề
                .skip(1)
                .filePath(filePath);

        return csvReader.readToObjects(values -> new VietLott645Entity()
                .setPeriod(Integer.parseInt(values[0].trim()))
                .setNumber1(Integer.parseInt(values[1].trim()))
                .setNumber2(Integer.parseInt(values[2].trim()))
                .setNumber3(Integer.parseInt(values[3].trim()))
                .setNumber4(Integer.parseInt(values[4].trim()))
                .setNumber5(Integer.parseInt(values[5].trim()))
                .setNumber6(Integer.parseInt(values[6].trim()))
                .setDateOfResult(DateUtil.toDate(values[7].trim(), PATTERN_YYMMDD_BLANK))
                .build());
    }

    public List<VietLott655Entity> readDataVietLott655(String filePath) {
        CsvReader csvReader = new SimpleCsvReader()
                .skip(1)
                .filePath(filePath);

        return csvReader.readToObjects(values -> new VietLott655Entity()
                .setPeriod(Integer.parseInt(values[0].trim()))
                .setNumber1(Integer.parseInt(values[1].trim()))
                .setNumber2(Integer.parseInt(values[2].trim()))
                .setNumber3(Integer.parseInt(values[3].trim()))
                .setNumber4(Integer.parseInt(values[4].trim()))
                .setNumber5(Integer.parseInt(values[5].trim()))
                .setNumber6(Integer.parseInt(values[6].trim()))
                .setDateOfResult(DateUtil.toDate(values[7].trim(), PATTERN_YYMMDD_BLANK))
                .build());
    }
}
