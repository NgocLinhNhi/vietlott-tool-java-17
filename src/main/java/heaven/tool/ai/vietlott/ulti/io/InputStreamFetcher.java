package heaven.tool.ai.vietlott.ulti.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
