package heaven.tool.ai.vietlott.ulti.excel_apache_poi;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ISimpleExcelWriter {

    XSSFWorkbook createXSSFWorkbook();

    SXSSFWorkbook createSXSSFWorkbook();

    void writeHeaderLine(XSSFSheet sheet, String headerTitles);

    void writeFileExcel(Boolean validateResult, Workbook workbook) throws Exception;

}
