package heaven.tool.ai.vietlott.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolLoop {

    private static final ThreadPoolLoop INSTANCE = new ThreadPoolLoop();
    public static ThreadPoolLoop getInstance() {
        return INSTANCE;
    }
    public ExecutorService newExecutorService(int threadPoolSize) {
        return Executors.newFixedThreadPool(threadPoolSize);
    }

}
