package heaven.tool.ai.vietlott.factory.concretes;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.dao.IVietLottDAO;
import heaven.tool.ai.vietlott.service.VietLottCommandImplements;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.service.BigDataHandlerService;


public class VietLott645Factory extends VietLottAbstractFactory {
    @Override
    public IVietLottDAO createVietLott() {
        return new VietLottCommandImplements(new BigDataHandlerService(VietLottType.VIETLOTT645));
    }
}
