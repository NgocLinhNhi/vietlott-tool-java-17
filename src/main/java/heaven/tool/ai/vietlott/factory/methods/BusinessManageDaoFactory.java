package heaven.tool.ai.vietlott.factory.methods;

import heaven.tool.ai.vietlott.factory.abstracts.VietLottAbstractFactory;
import heaven.tool.ai.vietlott.factory.concretes.VietLott645Factory;
import heaven.tool.ai.vietlott.factory.concretes.VietLott655Factory;
import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.HashMap;
import java.util.Map;

public class BusinessManageDaoFactory {
    private static BusinessManageDaoFactory INSTANCE;

    private final Map<VietLottType, VietLottAbstractFactory> vietLottHandler;

    public static BusinessManageDaoFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new BusinessManageDaoFactory();
        return INSTANCE;
    }

    private BusinessManageDaoFactory() {
        this.vietLottHandler = new HashMap<>();

        this.vietLottHandler.put(VietLottType.VIETLOTT645, new VietLott645Factory());
        this.vietLottHandler.put(VietLottType.VIETLOTT655, new VietLott655Factory());
    }

    public VietLottAbstractFactory getVietLottByType(VietLottType vietlottType) {
        return vietLottHandler.get(vietlottType);
    }
}
