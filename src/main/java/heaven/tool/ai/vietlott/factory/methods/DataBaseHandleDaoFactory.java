package heaven.tool.ai.vietlott.factory.methods;

import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.handler.RequestHandler;
import heaven.tool.ai.vietlott.handler.VietLott645InsertHandler;
import heaven.tool.ai.vietlott.handler.VietLott655InsertHandler;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class DataBaseHandleDaoFactory {
    private static DataBaseHandleDaoFactory INSTANCE;
    private final Map<VietLottType, RequestHandler> vietLottHandler;

    public static DataBaseHandleDaoFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new DataBaseHandleDaoFactory();
        return INSTANCE;
    }

    private DataBaseHandleDaoFactory() {
        this.vietLottHandler = new HashMap<>();
        this.vietLottHandler.put(VietLottType.VIETLOTT645, new VietLott645InsertHandler());
        this.vietLottHandler.put(VietLottType.VIETLOTT655, new VietLott655InsertHandler());
    }

    public RequestHandler getVietLottByType(VietLottType vietlottType) {
        return vietLottHandler.get(vietlottType);
    }
}
