package heaven.tool.ai.vietlott.factory.methods;

import heaven.tool.ai.vietlott.handler.VietLott655ViewDataHandler;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.handler.VietLott645ViewDataHandler;
import heaven.tool.ai.vietlott.handler.ViewDataRequestHandler;

import java.util.HashMap;
import java.util.Map;

public class VietLottResultFactory {
    private static VietLottResultFactory INSTANCE;

    private final Map<VietLottType, ViewDataRequestHandler> vietlottViewNumberHandler;

    public static VietLottResultFactory getInstance() {
        if (INSTANCE == null) INSTANCE = new VietLottResultFactory();
        return INSTANCE;
    }

    private VietLottResultFactory() {
        this.vietlottViewNumberHandler = new HashMap<>();

        this.vietlottViewNumberHandler.put(VietLottType.VIETLOTT645, new VietLott645ViewDataHandler());
        this.vietlottViewNumberHandler.put(VietLottType.VIETLOTT655, new VietLott655ViewDataHandler());
    }

    public ViewDataRequestHandler getVietLottByType(VietLottType vietlottType) {
        return vietlottViewNumberHandler.get(vietlottType);
    }
}
