package heaven.tool.ai.vietlott.factory.abstracts;

import heaven.tool.ai.vietlott.dao.IVietLottDAO;

// abstract factory design pattern
// Hoặc có thể dùng abstract factory = Interface như 1 abstract factory
// public interface  VietLottAbstractFactory
public abstract class VietLottAbstractFactory {

    public abstract IVietLottDAO createVietLott();

}
