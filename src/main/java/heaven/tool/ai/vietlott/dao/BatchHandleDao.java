package heaven.tool.ai.vietlott.dao;

import heaven.tool.ai.vietlott.load_config.DBConfig;
import heaven.tool.ai.vietlott.load_config.ExecutorConfig;
import heaven.tool.ai.vietlott.load_config.SQLQuerry;
import heaven.tool.ai.vietlott.ulti.MyPartition;
import heaven.tool.ai.vietlott.ulti.date.DateUtil;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.thread.VietLottExecuteCallable;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static heaven.tool.ai.vietlott.constant.DateConstant.PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS;

//java 17 sealed -permits class
public final class BatchHandleDao implements IBatchDao<VietLott645Entity> {

    private final Logger logger = LoggerFactory.getLogger(BatchHandleDao.class);

    public static BatchHandleDao getInstance() {
        return new BatchHandleDao();
    }

    @Override
    public void truncateDataBase(VietLottType vietLottType) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.execute(vietLottType == VietLottType.VIETLOTT655 ?
                SQLQuerry.TRUNCATE_TABLE_VIETLOTT_655 : SQLQuerry.TRUNCATE_TABLE_VIETLOTT_645);
    }

    //Insert data bình thường không batch không thread
    @Override
    public void insert(List<VietLott645Entity> listData, String tableName) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        for (VietLott645Entity data : listData) {
            jdbcTemplate.update(SQLQuerry.InsertVietLottSQL(tableName).toString(),
                    data.getPeriod(),
                    data.getNumber1(),
                    data.getNumber2(),
                    data.getNumber3(),
                    data.getNumber4(),
                    data.getNumber5(),
                    data.getNumber6(),
                    DateUtil.convertDateUtilToDateSQL(data.getDateOfResult()),
                    DateUtil.toString(new Date(), PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS)
            );
        }
    }

    //@Transactional = sai 1 công đoạn rollback toàn bộ list insert
    //Insert = Batch bình thường không thread + Partition
    @Transactional
    @Override
    public int[][] insertBatch(List<VietLott645Entity> listData,
                               int batchSize,
                               String tableName) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.batchUpdate(
                SQLQuerry.InsertVietLottSQL(tableName).toString(),
                listData,
                batchSize,
                (preparedStatement, data) -> {
                    preparedStatement.setInt(1, data.getPeriod());
                    preparedStatement.setInt(2, data.getNumber1());
                    preparedStatement.setInt(3, data.getNumber2());
                    preparedStatement.setInt(4, data.getNumber3());
                    preparedStatement.setInt(5, data.getNumber4());
                    preparedStatement.setInt(6, data.getNumber5());
                    preparedStatement.setInt(7, data.getNumber6());
                    preparedStatement.setDate(8, DateUtil.convertDateUtilToDateSQL(data.getDateOfResult()));
                    preparedStatement.setString(9, DateUtil.toString(
                            new Date(), PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS));
                });
        //int[][] : [] đầu = lần insert batch thứ 1,2,3  => [] sau = số bản ghi insert thành công
    }

    //Insert = Multi Thread (Executor) + Batch + Partition  => dont use desgin pattern
    @Transactional
    @Override
    public void insertExecutorBatch(List<VietLott645Entity> listData,
                                    int batchSize,
                                    String tableName) throws Exception {
        ExecutorService generator = Executors.newFixedThreadPool(ExecutorConfig.threadPoolSize);
        List<List<VietLott645Entity>> partition = MyPartition.partition(listData, batchSize);

        //C1: Classic code
        // partition có size bao nhiêu thì là bây nhiêu Thread
//        for (List<VietLott645Entity> listVietLott645 : partition) {
//            generator.execute(() -> {
//                insertBatch(listVietLott645, batchSize, jdbcTemplate);
//            });
//        }

        //C2 : code = Stream
        //partition có size bao nhiêu thì là bây nhiêu Thread
        partition.forEach(listVietLott645 -> generator.execute(
                () -> {
                    try {
                        insertBatch(listVietLott645, batchSize, tableName);
                    } catch (Exception e) {
                        logger.error("Handle Insert with thread Batch has error {0}", e);
                    }
                }));

        generator.shutdown();
        boolean finished = generator.awaitTermination(1, TimeUnit.DAYS);
        if (finished)
            logger.info("Finish generation");
        else
            logger.error("Generate report take more than one day. Stop");
    }

    //Sử dụng Executor + Future - không desgin pattern
    //Cách PMTC-VietTel sử dụng => get ra để biết Executor service nào đã xong check được
    //Kêt quả và tốc độ như nhau = insertExecutorBatch()
    //Insert = Partition + Multi Thread (Executor) + Use Futures +  Batch  => dont use desgin pattern
    @Transactional
    @Override
    public void insertFuturesBatch(List<VietLott645Entity> listData,
                                   int batchSize,
                                   String tableName) throws Exception {
        ExecutorService generator = Executors.newFixedThreadPool(ExecutorConfig.threadPoolSize);
        List<List<VietLott645Entity>> partition = MyPartition.partition(listData, batchSize);

        Collection<Future<?>> futures = new LinkedList<>();

        for (List<VietLott645Entity> lst : partition) {
            futures.add(generator.submit(() -> {
                try {
                    insertBatch(lst, batchSize, tableName);
                } catch (Exception e) {
                    logger.error("Handle with database has error {0}", e);
                }
            }));
        }

        for (Future<?> future : futures) {
            System.out.println("wait");
            future.get();
        }

        generator.shutdown();
        boolean finished = generator.awaitTermination(1, TimeUnit.DAYS);
        if (finished)
            logger.info("Finish generation");
        else
            logger.error("Generate report take more than one day. Stop");
    }

    //Insert = Partition + Multi Thread (Executor + Future) + Batch   => use design pattern tách class
    @Transactional
    @Override
    public void insertThreadBatchPattern(List<VietLott645Entity> listData,
                                         int batchSize,
                                         String tableName) throws Exception {
        VietLottExecuteCallable.getInstance().start(listData, batchSize, tableName);

    }

}


