package heaven.tool.ai.vietlott.dao;

import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.model.VietLott655Entity;

import java.util.List;

public interface IVietLottDAO {

    //lấy kết quả trong 1 tuần gần nhất
    Integer getNearestResultInWeek(VietLottType vietLottType, int numberInput, int dateDiff) throws Exception;

    void insertDataVietLott645(List<VietLott645Entity> obj) throws Exception;

    void insertDataVietLott655(List<VietLott655Entity> listData) throws Exception;

    void insertDataVietLottByFileExcel(List<VietLott645Entity> obj) throws Exception;

    void truncateData() throws Exception;

    //Load All Data for export csv file big data
    List<VietLott645Entity> loadAllData(VietLottType vietLottType) throws Exception;

    List<VietLott645Entity> loadAllDataByLimitOffSet(VietLottType vietLottType, int limit, int offset) throws Exception;

    //Number 1 information
    List<?> selectNumber2ByNumber1(VietLottType vietLottType, int number_1) throws Exception;

    List<?> selectNumber3ByNumber1(VietLottType vietLottType, int number_1) throws Exception;

    List<?> selectNumber4ByNumber1(VietLottType vietLottType, int number_1) throws Exception;

    List<?> selectNumber5ByNumber1(VietLottType vietLottType, int number_1) throws Exception;

    List<?> selectNumber6ByNumber1(VietLottType vietLottType, int number_1) throws Exception;

    //Number 2 information

    List<?> selectNumber3ByNumber2(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber4ByNumber2(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber5ByNumber2(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber6ByNumber2(VietLottType vietLottType, int number_2) throws Exception;

    //Number 3 information

    List<?> selectNumber4ByNumber3(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber5ByNumber3(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber6ByNumber3(VietLottType vietLottType, int number_2) throws Exception;

    //Number 4 information

    List<?> selectNumber5ByNumber4(VietLottType vietLottType, int number_2) throws Exception;

    List<?> selectNumber6ByNumber4(VietLottType vietLottType, int number_2) throws Exception;

    //Number 5 information
    List<?> selectNumber6ByNumber5(VietLottType vietLottType, int number_2) throws Exception;
}
