package heaven.tool.ai.vietlott.dao;

import heaven.tool.ai.vietlott.enums.VietLottType;
import heaven.tool.ai.vietlott.load_config.DBConfig;
import heaven.tool.ai.vietlott.load_config.SQLQuerry;
import heaven.tool.ai.vietlott.mapper.VietLott645Mapper;
import heaven.tool.ai.vietlott.mapper.VietLott655Mapper;
import heaven.tool.ai.vietlott.mapper.VietLottBigDataMapper;
import heaven.tool.ai.vietlott.model.VietLott645Entity;
import heaven.tool.ai.vietlott.prototype.DataBaseUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

import static heaven.tool.ai.vietlott.constant.Constant.*;

public interface VietLottDao extends IVietLottDAO {

    @Override
    //Java 9 default method of interface
    default Integer getNearestResultInWeek(VietLottType vietLottType, int numberInput, int dateDiff) throws Exception {
        useJdbcTemplate(vietLottType, numberInput, dateDiff);
        return useNamedParameterJdbcTemplate(vietLottType, numberInput, dateDiff);
    }

    //Java 9 private method interface => khai báo method có handle business in interface class
    private void useJdbcTemplate(VietLottType vietLottType, int numberInput, int dateDiff) throws Exception {
        //C1 : dùng jdbcTemplate (với trường hợp 1 parameter truyền cho nhiều vị trí trong câu sql )
        //nhược điểm nhìn câu sql append hơi rối mắt
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        jdbcTemplate.queryForObject(
                SQLQuerry.findNearestResultByDateDiff(
                        DataBaseUtil.getTableName(vietLottType),
                        numberInput,
                        dateDiff).toString(),
                Integer.class);
    }

    //Java 9 private method interface => khai báo method có handle business in interface class
    private Integer useNamedParameterJdbcTemplate(VietLottType vietLottType, int numberInput, int dateDiff) throws Exception {
        //C2 : dùng NamedParameterJdbcTemplate để set parameter trùng nhau số lượng nhiều trong 1 câu sql
        //ưu điểm nhìn câu sql không rối mắt
        //không truyền tham số như kiểu query được => sẽ thành truyền mấy chục tham số
        NamedParameterJdbcTemplate namedConnection = DBConfig.getInstance().getNamedConnection();
        return namedConnection.queryForObject(
                SQLQuerry.findNearestResultByDateDiff(DataBaseUtil.getTableName(vietLottType)).toString(),
                new MapSqlParameterSource()
                        .addValue("numberInput", numberInput)
                        .addValue("dateDiff", dateDiff),
                Integer.class);
    }

    //Number 1 Information
    //List<?> return nay ko bi warning khi complie voi List<VietLott645Entity>
    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber2ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_1,
                NUMBER_2),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber3ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_1,
                NUMBER_3),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber4ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_1,
                NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber5ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_1,
                NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber6ByNumber1(VietLottType vietLottType, int number_1) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_1,
                NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_1);
    }

    //Number 2 information
    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber3ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_2,
                NUMBER_3),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber4ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_2,
                NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber5ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_2,
                NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber6ByNumber2(VietLottType vietLottType, int number_2) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_2,
                NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_2);
    }

    //Number 3 information
    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber4ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_3,
                NUMBER_4),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber5ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_3,
                NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber6ByNumber3(VietLottType vietLottType, int number_3) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_3,
                NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_3);
    }

    //Number 4 information
    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber5ByNumber4(VietLottType vietLottType, int number_4) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_4,
                NUMBER_5),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_4);
    }

    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber6ByNumber4(VietLottType vietLottType, int number_4) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_4,
                NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_4);
    }

    //Number 5 information
    @Override
    @SuppressWarnings("unchecked")
    default List<?> selectNumber6ByNumber5(VietLottType vietLottType, int number_5) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.searchNumberByNumber(
                DataBaseUtil.getTableName(vietLottType),
                NUMBER_5,
                NUMBER_6),
                vietLottType == VietLottType.VIETLOTT645 ? new VietLott645Mapper() : new VietLott655Mapper(),
                number_5);
    }

    @Override
    default List<VietLott645Entity> loadAllData(VietLottType vietLottType) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.loadAllData(
                DataBaseUtil.getTableName(vietLottType)).toString(),
                new VietLottBigDataMapper());

    }

    @Override
    default List<VietLott645Entity> loadAllDataByLimitOffSet(VietLottType vietLottType,
                                                             int limit,
                                                             int offset) throws Exception {
        JdbcTemplate jdbcTemplate = DBConfig.getInstance().getConnection();
        return jdbcTemplate.query(SQLQuerry.loadAllDataByLimitOffSet(
                DataBaseUtil.getTableName(vietLottType), limit, offset).toString(),
                new VietLottBigDataMapper());

    }
}
