package heaven.tool.ai.vietlott.dao;

import heaven.tool.ai.vietlott.enums.VietLottType;

import java.util.List;

//java 17 sealed class
public sealed interface IBatchDao<T> permits BatchHandleDao {

    void truncateDataBase(VietLottType vietLottType) throws Exception;

    void insert(List<T> listData, String tableName) throws Exception;

    int[][] insertBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertExecutorBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertFuturesBatch(List<T> listData, int batchSize, String tableName) throws Exception;

    void insertThreadBatchPattern(List<T> listData, int batchSize, String tableName) throws Exception;
}
